package app

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/apollo/cmd/apollo/app/options"
	"bitbucket.org/mathildetech/app"
)

const commandDesc = `The application is a WEB server. In addition to providing services for front-end
static files, it also provides a query interface for Alauda global configuration.`

// NewApp creates a new alauda-console app
func NewApp(name string) *app.App {
	opts := options.NewOptions(name)
	application := app.NewApp("Alauda Container Enterprise Console",
		name,
		app.WithOptions(opts),
		app.WithDescription(commandDesc),
		app.WithRunFunc(run(opts)),
	)
	return application
}

func run(opts *options.Options) app.RunFunc {
	return func(basename string) error {
		srv := server.New(basename)
		err := opts.ApplyToServer(srv)
		if err != nil {
			return err
		}

		srv.Start()
		return nil
	}
}
