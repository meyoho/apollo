package options

import (
	"fmt"
	"log"

	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/apollo/client"
	"bitbucket.org/mathildetech/apollo/pkg/auth"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	flagLdapConnTimeout = "ldap-conn-timeout"
	flagSyncLdapTimeout = "syn-ldap-timeout"
	flagApiTimeout      = "api-timeout"
	flagApiQps          = "api-qps"
	flagApiBurst        = "api-burst"
	flagLabelBaseDomain = "label-base-domain"

	configLabelBaseDomain = "label.basedomain"
)

// ConsoleOptions contains configuration items related to console attributes.
type AuthOptions struct {
	// APIAddress string
	// AssetsPath string
	*auth.APIHandler
}

// NewConsoleOptions creates a ConsoleOptions object with default parameters.
func NewAuthOptions() *AuthOptions {

	handler := auth.CreateHTTPAPIHandler()
	return &AuthOptions{
		APIHandler: handler,
	}
}

// AddFlags adds flags for console to the specified FlagSet object.
func (o *AuthOptions) AddFlags(fs *pflag.FlagSet) {
	//fs.String(flagAPIAddress, o.APIAddress,
	//	"The address of the api gateway, including the scheme, host, and port.")
	//_ = viper.BindPFlag(configAPIAddress, fs.Lookup(flagAPIAddress))
	fs.Int(flagLdapConnTimeout, 2, "Used to set the timeout period for connect to ldap server")
	fs.Int(flagSyncLdapTimeout, 60, "Used to set the timeout period for retrieve entries from ldap server")
	fs.Int(flagApiTimeout, 2, "Used to set the timeout period for connect to ldap server")
	fs.Int(flagApiQps, 100, "QPS to use while talking with kubernetes apiserver")
	fs.Int(flagApiBurst, 100, "Burst to use while talking with kubernetes apiserver")
	fs.String(flagLabelBaseDomain, "alauda.io", "The based domain of the resource label")
	_ = viper.BindPFlag(auth.ConfigLdapConnTimeout, fs.Lookup(flagLdapConnTimeout))
	_ = viper.BindPFlag(auth.ConfigSyncLdapTimeout, fs.Lookup(flagSyncLdapTimeout))
	_ = viper.BindPFlag(auth.ConfigApiTimeout, fs.Lookup(flagApiTimeout))
	_ = viper.BindPFlag(auth.ConfigApiQps, fs.Lookup(flagApiQps))
	_ = viper.BindPFlag(auth.ConfigApiBurst, fs.Lookup(flagApiBurst))
	_ = viper.BindPFlag(configLabelBaseDomain, fs.Lookup(flagLabelBaseDomain))
}

// ApplyFlags parsing parameters from the command line or configuration file
// to the options instance.
func (o *AuthOptions) ApplyFlags() []error {
	var errs []error

	//o.APIAddress = viper.GetString(configAPIAddress)
	//
	//if o.APIAddress == "" {
	//	errs = append(errs, fmt.Errorf("--%s must be specified", flagAPIAddress))
	//}

	constant.PopulateWithLabelBaseDomain(viper.GetString(configLabelBaseDomain))
	auth.LabelProductInstallable = fmt.Sprintf(auth.LabelProductInstallable, viper.GetString(configLabelBaseDomain))
	auth.LabelProductIsDisplay = fmt.Sprintf(auth.LabelProductIsDisplay, viper.GetString(configLabelBaseDomain))
	return errs
}

// ApplyToServer apply options on server
func (o *AuthOptions) ApplyToServer(srv server.Server) error {
	// init k8s client
	clientManager := client.NewClientManager("", "")
	versionInfo, err := clientManager.InsecureClient().Discovery().ServerVersion()
	if err != nil {
		handleFatalInitError(err)
	}

	log.Printf("Successful initial request to the apiserver, version: %s", versionInfo.String())

	return o.APIHandler.ApplyToServer(srv, clientManager)
}

/**
 * Handles fatal init error that prevents server from doing any work. Prints verbose error
 * message and quits the server.
 */
func handleFatalInitError(err error) {
	log.Fatalf("Error while initializing connection to Kubernetes apiserver. "+
		"This most likely means that the cluster is misconfigured (e.g., it has "+
		"invalid apiserver certificates or service account's configuration) or the "+
		"--apiserver-host param points to a server that does not exist. Reason: %s\n"+
		"Refer to our FAQ and wiki pages for more information: "+
		"https://github.com/kubernetes/dashboard/wiki/FAQ", err)
}
