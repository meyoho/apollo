package options

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server/options"
)

// Options options for alauda-console
type Options struct {
	options.Optioner
}

// NewOptions new options for alauda-console
func NewOptions(name string) *Options {
	// recommended := options.NewRecommendedOptions().Options
	return &Options{
		Optioner: options.With(
			options.NewLogOptions(),
			options.NewInsecureServingOptions(),
			options.NewMetricsOptions(),
			options.NewErrorOptions(),
			NewAuthOptions(),
			options.NewOpenAPIOptions(),
		),
		// Optioner: options.With(
		// 	NewConsoleOptions(),
		// 	NewOIDCOptions(),
		// recommended.Options...,
		// ),
	}
}
