FROM alpine:3.11 as builder

COPY ./output  /opt/output

WORKDIR /opt/output

# pick the architecture
RUN ARCH= && dpkgArch="$(arch)" \
  && case "${dpkgArch}" in \
    x86_64) ARCH='amd64';; \
    aarch64) ARCH='arm64';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  && cp linux/${ARCH}/apollo apollo

FROM golang:1.13-alpine

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories

ENV TZ=Asia/Shanghai

RUN apk --no-cache --update add supervisor && mkdir -p /var/log/mathilde
WORKDIR /apollo

COPY ./conf/supervisord.conf /etc/supervisord.conf
COPY ./apollo.sh /apollo/apollo.sh

RUN chmod +x /apollo/*.sh

CMD ["/apollo/apollo.sh"]

ENTRYPOINT ["/bin/sh", "-c", "/apollo/apollo.sh \"$0\" \"$@\""]

COPY --from=builder /opt/output/apollo /apollo/apollo
