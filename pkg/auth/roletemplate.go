package auth

import (
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"fmt"
	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"log"
	"net/http"
)

func (apiHandler *APIHandler) HandleDeleteRoleTemplate(request *restful.Request, response *restful.Response) {
	roleTemplateName := request.PathParameter("name")
	log.Printf("HandleDeleteRoleTemplate roleTemplateName:%s", roleTemplateName)

	// get crdClient
	crdClientV1beta1, err := apiHandler.cManager.AuthV1Beta1Client(request)
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}
	crdClientV1, err := apiHandler.cManager.InsecureAuthV1Client()
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	roleTemplate, err := crdClientV1beta1.RoleTemplates().Get(roleTemplateName, metav1.GetOptions{})
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	// official roleTemplate can't be delete
	if roleTemplate.IsOfficial() {
		err := errors.NewBadRequest("official roleTemplate can't be delete")
		apiHandler.WriteError(response, err)
		return
	}

	if err = crdClientV1beta1.RoleTemplates().Delete(roleTemplateName, &metav1.DeleteOptions{}); err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	var gracePeriodSecondsZero int64 = 0
	labelSelector := fmt.Sprintf("%s,%s=%s", rc.LabelRoleName, rc.LabelRoleName, roleTemplateName)
	err = crdClientV1.UserBindings().DeleteCollection(&metav1.DeleteOptions{
		GracePeriodSeconds: &gracePeriodSecondsZero}, metav1.ListOptions{
		LabelSelector: labelSelector,
	})
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	response.WriteHeader(http.StatusOK)
}
