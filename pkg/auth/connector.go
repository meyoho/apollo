package auth

import (
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"go.uber.org/multierr"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"net"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/mathildetech/apollo/pkg/auth/idp"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/ldap"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/oidc"

	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/log"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/storage"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	clientapi "bitbucket.org/mathildetech/apollo/client/api"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"

	"github.com/emicklei/go-restful"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

const (
	ConfigLdapConnTimeout = "ldap.conn-timeout"

	ConnectorNameMaxLength = 60
)

// handleSyncLdap will sync the user between crd and ldap
func (apiHandler *APIHandler) HandleConnectors(request *restful.Request, res *restful.Response) {
	wrapper := &idp.ConnectorWrapper{}
	if err := request.ReadEntity(wrapper); err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	conn := wrapper.ToConnector()
	// conn.ID and conn.ObjectMeta.Name are equal to "名称"
	// conn.Name is equals to "显示名称"
	if len(conn.ID) > ConnectorNameMaxLength {
		apiHandler.WriteError(res, fmt.Errorf("Name of connector should be no more than %d", ConnectorNameMaxLength))
		return
	}

	config, err := getDexConfig(apiHandler.cManager, request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	dexLogger, _ = log.NewLogger(config.Logger.Level, config.Logger.Format)

	connConfig := conn.Config
	client, err := connConfig.Open(conn.ID, dexLogger)
	if err != nil {
		logger.Error("Failed to open connector with specified config", zap.Any("name", conn.Name), zap.Any("id", conn.ID), zap.Any("err", err))
		apiHandler.WriteError(res, err)
		return
	}

	var ok bool

	switch connConfig.(type) {
	case *ldap.Config:
		err, ok = validateLDAP(request, client, wrapper.IDPValidation.Username, wrapper.IDPValidation.Password)
	case *oidc.Config:
		oidcConfig, _ := connConfig.(*oidc.Config)
		err, ok = validateOIDC(oidcConfig, wrapper.IDPValidation.Username, wrapper.IDPValidation.Password)
	default:
		apiHandler.WriteError(res, errors.New("Invalid connector type"))
		return
	}

	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	if !ok {
		logger.Error("Invalid username or password when validate the tobe created connector", zap.Any("name", conn.Name), zap.Any("id", conn.ID))
		apiHandler.WriteError(res, errors.New("Invalid username or password"))
		return
	}

	storageClient, err := config.Storage.Config.Open(dexLogger)
	if err != nil {
		logger.Error("failed to initialize storage", zap.Any("err", err))
		apiHandler.WriteError(res, err)
		return
	}

	storageConnector, _ := idp.ToStorageConnector(conn)

	switch request.Request.Method {
	case http.MethodPost:
		err = storageClient.CreateConnector(storageConnector)
	case http.MethodPut:
		connectorId := request.PathParameter("connector-id")
		updater := func(source storage.Connector) (storage.Connector, error) {
			source.Name = storageConnector.Name
			source.Type = storageConnector.Type
			source.Config = storageConnector.Config
			source.ObjectMeta = storageConnector.ObjectMeta
			return source, nil
		}
		err = storageClient.UpdateConnector(connectorId, updater)
	}
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	createdConnector, _ := storageClient.GetConnector(conn.ID)
	result, _ := idp.FromStorageConnector(createdConnector)

	res.WriteHeaderAndEntity(http.StatusOK, result)
}

func canIActionConnector(handler *APIHandler, request *restful.Request, action string) bool {
	leaderElectionNamespace := GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	selfSubjectAccessReview := clientapi.ToSelfSubjectAccessReview(leaderElectionNamespace, "", "connectors", "dex.coreos.com", action)
	return handler.cManager.CanI(request, selfSubjectAccessReview)
}

func (apiHandler *APIHandler) HandleDeleteConnectors(request *restful.Request, res *restful.Response)  {

	if !canIActionConnector(apiHandler, request, "delete") {
		err := k8serrors.NewForbidden(schema.GroupResource{Group:"dex.coreos.com", Resource:"connectors"}, "", nil)
		apiHandler.WriteError(res, err)
	}

	config, err := getDexConfig(apiHandler.cManager, request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	dexLogger, err = log.NewLogger(config.Logger.Level, config.Logger.Format)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	storageClient, err := config.Storage.Config.Open(dexLogger)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	connectorId := request.PathParameter("connector-id")
	conn, err := storageClient.GetConnector(connectorId)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	labelSelectorStr := fmt.Sprintf("%s,%s=%s,%s,%s=%s", rc.LabelUserConnectorType, rc.LabelUserConnectorType,
		conn.Type, rc.LabelUserConnectorID, rc.LabelUserConnectorID, conn.ID)
	labelSelector, err := labels.Parse(labelSelectorStr)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	crdClient, err := apiHandler.cManager.InsecureAuthV1Client()
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	if deleteUser, _ := strconv.ParseBool(request.QueryParameter("delete-users")); deleteUser {
		err = crdClient.Users().DeleteCollection(&metav1.DeleteOptions{}, metav1.ListOptions{
			LabelSelector: labelSelector.String(),
		})
		if err != nil {
			apiHandler.WriteError(res, err)
			return
		}
	}else {
		users, err := crdClient.Users().List(metav1.ListOptions{
			LabelSelector: labelSelector.String(),
		})
		if err != nil {
			apiHandler.WriteError(res, err)
			return
		}

		var wg sync.WaitGroup

		ch := make(chan error, len(users.Items))

		for _, u := range users.Items {
			wg.Add(1)
			go func(user authv1.User, c chan error, w *sync.WaitGroup) {
				defer w.Done()

				user.Spec.Valid = false
				_, err = crdClient.Users().Update(&user)
				if err != nil {
					c <- err
				}
			}(u, ch, &wg)
		}
		if waitTimeout(&wg, time.Second*5) {
			apiHandler.WriteError(res, errors.New("update users timeout"))
			return
		}
		close(ch)
		if len(ch) > 0 {
			var errs error
			for e := range ch {
				errs = multierr.Append(errs, e)
			}
			apiHandler.WriteError(res, errs)
			return
		}
	}

	// delete connector
	err = storageClient.DeleteConnector(connectorId)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	res.WriteHeader(http.StatusOK)
}

func waitTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	c := make(chan struct{})
	go func() {
		wg.Wait()
		close(c)
	}()
	select {
	case <-c:
		return false // completed normally
	case <-time.After(timeout):
		return true // timed out
	}
}

// validateLDAP check the remote client and check if username/password is valid
func validateLDAP(request *restful.Request, client connector.Connector, username, password string) (error, bool) {
	if username == "" && password == "" {
		return nil, true
	}
	var err error
	passwordConnector, ok := client.(connector.PasswordConnector)
	if !ok {
		err = errors.New("Invalid Ldap Config")
		return err, false
	}

	// hardcode scopes
	scopes := connector.Scopes{
		OfflineAccess: false,
		Groups:        false,
	}

	c := make(chan int, 1)

	go func() {
		_, ok, err = passwordConnector.Login(request.Request.Context(), scopes, username, password)
		c <- 1
	}()

	ldapConnTimeout := viper.GetInt(ConfigLdapConnTimeout)

	select {
	case <-c:
		break
	case <-time.After(time.Duration(ldapConnTimeout) * time.Second):
		ok = false
		err = errors.New("Timeout when validate the username/password pair")
	}

	if err != nil {
		logger.Error("error occured when validate the provided username and password", zap.Any("err", err))
	}
	return err, ok
}

// validateOIDC check the remote client and check if username/password is valid
func validateOIDC(config *oidc.Config, username, password string) (error, bool) {
	logger.Info("validateOIDC")
	if username == "" && password == "" {
		return nil, true
	}

	client := genHttpClient()

	response, err := client.Get(fmt.Sprintf("%s/.well-known/openid-configuration", config.Issuer))
	if err != nil {
		logger.Error("failed to request token endpoint when validate the provided username and password", zap.Any("err", err))
		return err, false
	}
	defer response.Body.Close()

	body, _ := ioutil.ReadAll(response.Body)

	var endpoints struct {
		AuthUrl  string `json:"authorization_endpoint"`
		TokenUrl string `json:"token_endpoint"`
	}
	err = json.Unmarshal(body, &endpoints)
	if err != nil {
		logger.Error("failed to request token endpoint when validate the provided username and password", zap.Any("err", err))
		return err, false
	}

	payloadStr := fmt.Sprintf(
		"client_id=%s&client_secret=%s&username=%s&password=%s&grant_type=password&response_type=token",
		config.ClientID, config.ClientSecret, username, password)
	contentType := "application/x-www-form-urlencoded"
	response, err = client.Post(endpoints.TokenUrl, contentType, strings.NewReader(payloadStr))
	if err != nil {
		logger.Error("failed to validate the provided username and password", zap.Any("err", err))
		return err, false
	}

	logger.Info("Validate the provided username and password with response status_code", zap.Any("statusCode", response.StatusCode))

	if response.StatusCode == http.StatusOK {
		return nil, true
	}

	return nil, false
}

func genHttpClient() *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			DialContext: (&net.Dialer{
				Timeout: time.Second * 2,
			}).DialContext,
			IdleConnTimeout:       90 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
}
