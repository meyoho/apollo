package auth

import (
	wg "alauda.io/warpgate"
	dexapi "bitbucket.org/mathildetech/dex/api"
	"fmt"
	"google.golang.org/grpc"
	"io/ioutil"
)

const (
	DexGrpc = "dex-clusterip:5557"
)

func newDexClient() (dexapi.DexClient, error)  {

	hostAndPort := DexGrpc

	conn, err := grpc.Dial(hostAndPort, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("dail: %v", err)
	}
	return dexapi.NewDexClient(conn), nil
}

func IsFeatureGateEnabled(featureGate string) (bool, error) {
	const (
		tokenFile = "/var/run/secrets/kubernetes.io/serviceaccount/token"
	)
	token, err := ioutil.ReadFile(tokenFile)
	if err != nil {
		return false, err
	}
	wwg := wg.NewWarpGate(wg.Config{
		AuthorizationToken: string(token),
		APIEndpoint:        "http://archon"})
	enabled, err := wwg.IsFeatureGateEnabled(featureGate)
	if err != nil {
		return false, err
	}
	return enabled, nil
}
