package auth

import (
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	dexapi "bitbucket.org/mathildetech/dex/api"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	restufl "github.com/emicklei/go-restful"
	"golang.org/x/crypto/bcrypt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAPIHandler_HandleCreateUser(t *testing.T) {
	t.Skipped()

	APIHandler := CreateHTTPAPIHandler()

	localUser := getTestUser()

	localUserBytes, _ := json.Marshal(localUser)
	bodyReader := bytes.NewReader(localUserBytes)

	t.Run("create a user", func(t *testing.T) {
		t.SkipNow()
		httpRequest, _ := http.NewRequest("POST", "/users", bodyReader) // POST and PUT body parameters take precedence over URL query string
		httpRequest.Header.Set("Content-Type", "application/json; charset=UTF-8")
		request := restufl.NewRequest(httpRequest)
		response := restufl.NewResponse(httptest.NewRecorder())
		APIHandler.HandleCreateUser(request, response)
	})

	t.Run("is have permissions for create user", func(t *testing.T) {
		t.SkipNow()
		userServer := UserServer{}
		server := httptest.NewServer(&userServer)
		httpRequest, _ := http.NewRequest("POST", server.URL, bodyReader) // POST and PUT body parameters take precedence over URL query string
		httpRequest.Header.Set("Content-Type", "application/json; charset=UTF-8")
		request := restufl.NewRequest(httpRequest)
		got := canIActionUser(APIHandler, request, "create")
		want := true
		if got != want {
			t.Errorf("want '%v' got '%v'", got, want)
		}
	})

	t.Run("read entity from requst", func(t *testing.T) {
		httpRequest, _ := http.NewRequest("POST", "/users", bodyReader) // POST and PUT body parameters take precedence over URL query string
		httpRequest.Header.Set("Content-Type", "application/json; charset=UTF-8")
		request := restufl.NewRequest(httpRequest)

		user := new(authv1.User)
		err := ReadUserEntity(request, user)
		if err != nil {
			t.Errorf("want nil got not nil")
		}
	})
}

func TestBuildCreatePassWordFromUser(t *testing.T) {

	localUser := getTestUser()

	t.Run("build a password", func(t *testing.T) {
		p, err := BuildCreatePasswordReqFromUser(localUser)
		if err != nil {
			t.Errorf("BuildCreatePassWordFromUser Error")
		}

		hash, err := base64.StdEncoding.DecodeString(localUser.Spec.Password)

		if err != nil {
			t.Errorf("DecodeString Error")
		}

		want := dexapi.CreatePasswordReq{
			Password: &dexapi.Password{
				Email:    localUser.Spec.Email,
				Hash:     hash,
				Username: localUser.Spec.Username,
				Account:  localUser.Spec.Account,
			},
		}

		checkBuildCreatePassWordFromUser(t, *p, want)
	})
}

func TestCheckUserSpec(t *testing.T) {
	localUser := getTestUser()

	t.Run("check user spec correct", func(t *testing.T) {
		err := CheckUserSpec(&localUser)
		if err != nil {
			t.Errorf("Check User Spec Error")
		}
	})

	t.Run("check user email empty", func(t *testing.T) {
		user := getTestUser()
		user.Spec.Email = ""
		got := CheckUserSpec(&user)
		want := errors.New("user email can't be empty")
		if got.Error() != want.Error() {
			t.Errorf("got '%s' not equal want '%s'", got.Error(), want.Error())
		}
	})

	t.Run("check user account empty", func(t *testing.T) {
		user := getTestUser()
		user.Spec.Account = ""
		got := CheckUserSpec(&user)
		want := errors.New("account can't be empty")
		if got.Error() != want.Error() {
			t.Errorf("got '%s' not equal want '%s'", got.Error(), want.Error())
		}
	})

	t.Run("check user account length", func(t *testing.T) {
		user := getTestUser()
		s := "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
		user.Spec.Account = fmt.Sprintf("%s%s%s%s%s%s%s%s%s", s, s, s, s, s, s, s, s, s)
		got := CheckUserSpec(&user)
		want := errors.New(fmt.Sprintf("account length mast less than %d", 253))
		if got.Error() != want.Error() {
			t.Errorf("got '%s' not equal want '%s'", got.Error(), want.Error())
		}
	})

	t.Run("check user plain password greater than 20 length", func(t *testing.T) {
		user := getTestUser()
		s := "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
		password := base64.StdEncoding.EncodeToString([]byte(s))
		user.Spec.Password = password
		got := CheckUserSpec(&user)
		want := errors.New("password don't only contains character")
		if got.Error() != want.Error() {
			t.Errorf("got '%s' not equal want '%s'", got.Error(), want.Error())
		}
	})

	t.Run("check user plain password less than 6 length", func(t *testing.T) {
		user := getTestUser()
		s := "abcde"
		password := base64.StdEncoding.EncodeToString([]byte(s))
		user.Spec.Password = password
		got := CheckUserSpec(&user)
		want := errors.New("password don't only contains character")
		if got.Error() != want.Error() {
			t.Errorf("got '%s' not equal want '%s'", got.Error(), want.Error())
		}
	})
}

type UserServer struct {
	Users []authv1.User
}

func (u *UserServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodPost {
		w.WriteHeader(http.StatusAccepted)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func checkBuildCreatePassWordFromUser(t *testing.T, got, want dexapi.CreatePasswordReq) {
	if got.Password.Account != want.Password.Account {
		t.Errorf("Account got '%v' want '%v'", got.Password.Account, want.Password.Account)
	}
	if got.Password.Username != want.Password.Username {
		t.Errorf("Username got '%v' want '%v'", got.Password.Username, want.Password.Username)
	}
	if got.Password.Email != want.Password.Email {
		t.Errorf("Email got '%v' want '%v'", got.Password.Username, want.Password.Username)
	}
	if err := bcrypt.CompareHashAndPassword(got.Password.Hash, want.Password.Hash); err != nil {
		t.Errorf("CompareHashAndPassword Error")
	}
}

func getTestUser() authv1.User {
	return authv1.User{
		TypeMeta: metav1.TypeMeta{
			Kind:       "User",
			APIVersion: "auth.alauda.io/v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Annotations: map[string]string{
				"alauda.io/display-name": "localuser-display-name",
			},
			Labels: map[string]string{
				"auth.alauda.io/user.connector_id":   "local",
				"auth.alauda.io/user.connector_type": "local",
				"auth.alauda.io/user.email":          "0308a145f3aa24e0d0e7919563addde5",
				"auth.alauda.io/user.username":       "",
				"auth.alauda.io/user.valid":          "true",
			},
			Name: "0308a145f3aa24e0d0e7919563addde5",
		},
		Spec: authv1.UserSpec{
			ConnectorName: "local",
			ConnectorType: "local",
			Email:         "localuser@alauda.io",
			Groups: []string{
				"ungrouped",
			},
			Username: "localuser-display-name",
			Account:  "localuser",
			Password: "YWJjMTIz",
		},
	}
}

func TestReadUserEntity(t *testing.T) {
	t.Run("read user with patch body", func(t *testing.T) {
		localUser := &authv1.User{
			Spec:authv1.UserSpec{
				OldPassword: "cGFzc3dvcmQ=",
				Password:"cGFzc3dvcmQ=",
			},
		}

		localUserBytes, _ := json.Marshal(localUser)
		bodyReader := bytes.NewReader(localUserBytes)

		httpRequest, _ := http.NewRequest(http.MethodPatch, "/users", bodyReader)
		httpRequest.Header.Set("Content-Type", "application/merge-patch+json")
		request := restufl.NewRequest(httpRequest)

		user := new(authv1.User)
		err := json.NewDecoder(request.Request.Body).Decode(user)
		if err != nil {
			t.Errorf("Read Patch User Entity Error")
		}
	})
}

func TestGetUpdatePasswordReq(t *testing.T) {

	t.Run("update password", func(t *testing.T) {
		userPassword := "YWJjNDU2"
		userOldPassword := "YWJjMTIz"
		email := "test01@test02.com"
		password := new(dexapi.Password)
		password.Email = email
		hash, err := bcrypt.GenerateFromPassword([]byte("abc123"), bcrypt.DefaultCost)
		if err != nil {
			t.Errorf("GenerateFromPassword Error")
		}
		password.Hash = hash
		updateReq, err := GetUpdatePasswordReq(password, userPassword, userOldPassword, nil)
		if err != nil {
			t.Errorf("GetUpdatePasswordReq Error")
		}
		if updateReq.Email != email {
			t.Errorf("got email '%s' want email '%s'", updateReq.Email, email)
		}
		err = bcrypt.CompareHashAndPassword(updateReq.NewHash, []byte("abc456"))
		if err != nil {
			t.Errorf("GetUpdatePasswordReq password error")
		}
	})

	t.Run("reset password", func(t *testing.T) {
		userPassword := "YWJjNDU2"
		email := "test01@test02.com"
		password := new(dexapi.Password)
		password.Email = email
		hash, err := bcrypt.GenerateFromPassword([]byte("abc123"), bcrypt.DefaultCost)
		if err != nil {
			t.Errorf("GenerateFromPassword Error")
		}
		password.Hash = hash
		updateReq, err := GetUpdatePasswordReq(password, userPassword, nil, nil)
		if err != nil {
			t.Errorf("GetUpdatePasswordReq Error")
		}
		if updateReq.Email != email {
			t.Errorf("got email '%s' want email '%s'", updateReq.Email, email)
		}
		err = bcrypt.CompareHashAndPassword(updateReq.NewHash, []byte("abc456"))
		if err != nil {
			t.Errorf("GetUpdatePasswordReq password error")
		}
	})

	t.Run("update display name", func(t *testing.T) {
		username := "test01"
		email := "test01@test02.com"
		password := new(dexapi.Password)
		password.Email = email
		updateReq, err := GetUpdatePasswordReq(password, nil, nil, username)
		if err != nil {
			t.Errorf("GetUpdatePasswordReq Error")
		}
		if updateReq.Email != email {
			t.Errorf("got email '%s' want email '%s'", updateReq.Email, email)
		}
		if updateReq.NewUsername != username {
			t.Errorf("got '%s' want '%s'", updateReq.NewUsername, username)
		}
	})
}
