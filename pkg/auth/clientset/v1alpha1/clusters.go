package v1alpha1

import (
	// "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"

	clusterv1alpha1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/clusterregistry/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ClusterInterface interface {
	List(opts metav1.ListOptions) (*clusterv1alpha1.ClusterList, error)
	Get(name string, options metav1.GetOptions) (*clusterv1alpha1.Cluster, error)
}

type clusterClient struct {
	ns     string
	client rest.Interface
}

func (c *clusterClient) List(opts metav1.ListOptions) (*clusterv1alpha1.ClusterList, error) {
	result := clusterv1alpha1.ClusterList{}
	err := c.client.
		Get().
		Namespace(c.ns).
		Resource("clusters").
		// SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1Alpha1).
		Do().
		Into(&result)

	return &result, err
}

func (c *clusterClient) Get(name string, opts metav1.GetOptions) (*clusterv1alpha1.Cluster, error) {
	result := clusterv1alpha1.Cluster{}
	err := c.client.
		Get().
		Namespace(c.ns).
		Resource("clusters").
		Name(name).
		// SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1Alpha1).
		Do().
		Into(&result)

	return &result, err
}
