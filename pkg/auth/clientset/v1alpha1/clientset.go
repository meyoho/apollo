package v1alpha1

import (
	"log"

	"k8s.io/apimachinery/pkg/runtime/schema"

	"k8s.io/apimachinery/pkg/runtime/serializer"

	clusterv1alpha1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/clusterregistry/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type ClusterV1Alpha1Interface interface {
	Clusters(ns string) ClusterInterface
}

type ClusterV1Alpha1Client struct {
	restClient rest.Interface
}

var versionV1Alpha1 = schema.GroupVersion{Version: "v1alpha1"}

var SchemeGroupVersion = schema.GroupVersion{Group: "clusterregistry.k8s.io", Version: "v1alpha1"}

func addKnownTypes(scheme *runtime.Scheme) error {
	scheme.AddKnownTypes(SchemeGroupVersion,
		&clusterv1alpha1.Cluster{},
		&clusterv1alpha1.ClusterList{},
	)
	metav1.AddToGroupVersion(scheme, SchemeGroupVersion)
	return nil
}

func NewForConfig(c *rest.Config) (*ClusterV1Alpha1Client, error) {

	schemes := runtime.NewScheme()
	SchemeBuilder := runtime.NewSchemeBuilder(addKnownTypes)
	if err := SchemeBuilder.AddToScheme(schemes); err != nil {
		log.Printf("AddToScheme: err: %+v", err)
	}

	config := *c
	config.ContentConfig.GroupVersion = &SchemeGroupVersion
	config.APIPath = "/apis"
	config.AcceptContentTypes = runtime.ContentTypeJSON
	config.ContentType = runtime.ContentTypeJSON
	if config.NegotiatedSerializer == nil {
		config.NegotiatedSerializer = serializer.DirectCodecFactory{CodecFactory: scheme.Codecs}
	}
	if config.UserAgent == "" {
		config.UserAgent = rest.DefaultKubernetesUserAgent()
	}

	client, err := rest.RESTClientFor(&config)
	if err != nil {
		return nil, err
	}

	return &ClusterV1Alpha1Client{restClient: client}, nil
}

func (c *ClusterV1Alpha1Client) Clusters(ns string) ClusterInterface {
	return &clusterClient{
		ns:     ns,
		client: c.restClient,
	}
}
