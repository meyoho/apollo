package v1beta1

import (
	authv1beta1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type RoleTemplateInterface interface {
	List(opts metav1.ListOptions) (*authv1beta1.RoleTemplateList, error)
	Get(name string, options metav1.GetOptions) (*authv1beta1.RoleTemplate, error)
	Create(*authv1beta1.RoleTemplate) (*authv1beta1.RoleTemplate, error)
	Delete(name string, options *metav1.DeleteOptions) error
}

type roletemplateClient struct {
	client rest.Interface
}

func (c *roletemplateClient) List(opts metav1.ListOptions) (*authv1beta1.RoleTemplateList, error) {
	result := authv1beta1.RoleTemplateList{}
	err := c.client.
		Get().
		Resource("roletemplates").
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1Beta1).
		Do().
		Into(&result)

	return &result, err
}

func (c *roletemplateClient) Get(name string, opts metav1.GetOptions) (*authv1beta1.RoleTemplate, error) {
	result := authv1beta1.RoleTemplate{}
	err := c.client.
		Get().
		Resource("roletemplates").
		Name(name).
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1Beta1).
		Do().
		Into(&result)

	return &result, err
}

func (c *roletemplateClient) Create(RoleTemplate *authv1beta1.RoleTemplate) (result *authv1beta1.RoleTemplate, err error) {
	result = &authv1beta1.RoleTemplate{}
	err = c.client.Post().
		Resource("roletemplates").
		Body(RoleTemplate).
		Do().
		Into(result)
	return
}

func (c *roletemplateClient) Delete(name string, options *metav1.DeleteOptions) error {
	return c.client.Delete().
		Resource("roletemplates").
		Name(name).
		Body(options).
		Do().
		Error()
}
