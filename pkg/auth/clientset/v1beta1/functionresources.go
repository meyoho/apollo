package v1beta1

import (
	authv1beta1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type FunctionResourceInterface interface {
	List(opts metav1.ListOptions) (*authv1beta1.FunctionResourceList, error)
	Get(name string, options metav1.GetOptions) (*authv1beta1.FunctionResource, error)
	Create(*authv1beta1.FunctionResource) (*authv1beta1.FunctionResource, error)
	Delete(name string, options *metav1.DeleteOptions) error
}

type functionResourceClient struct {
	client rest.Interface
}

func (c *functionResourceClient) List(opts metav1.ListOptions) (*authv1beta1.FunctionResourceList, error) {
	result := authv1beta1.FunctionResourceList{}
	err := c.client.
		Get().
		Resource("functionresources").
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1Beta1).
		Do().
		Into(&result)

	return &result, err
}

func (c *functionResourceClient) Get(name string, opts metav1.GetOptions) (*authv1beta1.FunctionResource, error) {
	result := authv1beta1.FunctionResource{}
	err := c.client.
		Get().
		Resource("functionresources").
		Name(name).
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1Beta1).
		Do().
		Into(&result)

	return &result, err
}

func (c *functionResourceClient) Create(FunctionResource *authv1beta1.FunctionResource) (result *authv1beta1.FunctionResource, err error) {
	result = &authv1beta1.FunctionResource{}
	err = c.client.Post().
		Resource("functionresources").
		Body(FunctionResource).
		Do().
		Into(result)
	return
}

func (c *functionResourceClient) Delete(name string, options *metav1.DeleteOptions) error {
	return c.client.Delete().
		Resource("functionresources").
		Name(name).
		Body(options).
		Do().
		Error()
}
