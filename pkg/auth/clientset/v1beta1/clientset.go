package v1beta1

import (
	"log"

	"k8s.io/apimachinery/pkg/runtime/schema"

	"k8s.io/apimachinery/pkg/runtime/serializer"

	authv1beta1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type AuthV1Beta1Interface interface {
	RoleTemplates() RoleTemplateInterface
	FunctionResources() FunctionResourceInterface
}

type AuthV1Beta1Client struct {
	restClient rest.Interface
}

var versionV1Beta1 = schema.GroupVersion{Version: "v1"}

var SchemeGroupVersion = authv1beta1.SchemeGroupVersion

func addKnownTypes(scheme *runtime.Scheme) error {
	log.Printf("Add Types")
	scheme.AddKnownTypes(SchemeGroupVersion,
		&authv1beta1.RoleTemplate{},
		&authv1beta1.RoleTemplateList{},
	)
	log.Printf("Add to GroupVersion")
	metav1.AddToGroupVersion(scheme, SchemeGroupVersion)
	log.Printf("scheme:%v", scheme.AllKnownTypes())
	return nil
}

func NewForConfig(c *rest.Config) (*AuthV1Beta1Client, error) {

	schemes := runtime.NewScheme()
	log.Printf("new ForConfig")
	SchemeBuilder := runtime.NewSchemeBuilder(addKnownTypes)
	if err := SchemeBuilder.AddToScheme(schemes); err != nil {
		log.Printf("AddToScheme: err: %+v", err)
	}

	config := *c
	config.ContentConfig.GroupVersion = &SchemeGroupVersion
	config.APIPath = "/apis"
	config.AcceptContentTypes = runtime.ContentTypeJSON
	config.ContentType = runtime.ContentTypeJSON
	if config.NegotiatedSerializer == nil {
		config.NegotiatedSerializer = serializer.DirectCodecFactory{CodecFactory: scheme.Codecs}
	}
	if config.UserAgent == "" {
		config.UserAgent = rest.DefaultKubernetesUserAgent()
	}

	client, err := rest.RESTClientFor(&config)
	if err != nil {
		return nil, err
	}

	return &AuthV1Beta1Client{restClient: client}, nil
}

func (c *AuthV1Beta1Client) RoleTemplates() RoleTemplateInterface {
	return &roletemplateClient{
		client: c.restClient,
	}
}

func (c *AuthV1Beta1Client) FunctionResources() FunctionResourceInterface {
	return &functionResourceClient{
		client: c.restClient,
	}
}
