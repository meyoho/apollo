package v1

import (
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type UserBindingInterface interface {
	List(opts metav1.ListOptions) (*authv1.UserBindingList, error)
	Get(name string, options metav1.GetOptions) (*authv1.UserBinding, error)
	Create(*authv1.UserBinding) (*authv1.UserBinding, error)
	Delete(name string, options *metav1.DeleteOptions) error
	DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error
}

type userbindingClient struct {
	client rest.Interface
}

func (c *userbindingClient) List(opts metav1.ListOptions) (*authv1.UserBindingList, error) {
	result := authv1.UserBindingList{}
	err := c.client.
		Get().
		Resource("userbindings").
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}

func (c *userbindingClient) Get(name string, opts metav1.GetOptions) (*authv1.UserBinding, error) {
	result := authv1.UserBinding{}
	err := c.client.
		Get().
		Resource("userbindings").
		Name(name).
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}

func (c *userbindingClient) Create(userBinding *authv1.UserBinding) (result *authv1.UserBinding, err error) {
	result = &authv1.UserBinding{}
	err = c.client.Post().
		Resource("userbindings").
		Body(userBinding).
		Do().
		Into(result)
	return
}

func (c *userbindingClient) Delete(name string, options *metav1.DeleteOptions) error {
	return c.client.Delete().
		Resource("userbindings").
		Name(name).
		Body(options).
		Do().
		Error()
}

func (c *userbindingClient) DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error {
	return c.client.Delete().
		Resource("userbindings").
		SpecificallyVersionedParams(&listOptions, scheme.ParameterCodec, versionV1).
		Body(options).
		Do().
		Error()
}
