package v1

import (
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"

	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ProjectInterface interface {
	List(opts metav1.ListOptions) (*authv1.ProjectList, error)
	Get(name string, options metav1.GetOptions) (*authv1.Project, error)
}

type projectClient struct {
	client rest.Interface
}

func (c *projectClient) List(opts metav1.ListOptions) (*authv1.ProjectList, error) {
	result := authv1.ProjectList{}
	err := c.client.
		Get().
		Resource("projects").
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}

func (c *projectClient) Get(name string, opts metav1.GetOptions) (*authv1.Project, error) {
	result := authv1.Project{}
	err := c.client.
		Get().
		Resource("projects").
		Name(name).
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}
