package v1

import (
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"

	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
)

type UserInterface interface {
	Create(*authv1.User) (*authv1.User, error)
	List(opts metav1.ListOptions) (*authv1.UserList, error)
	Update(*authv1.User) (*authv1.User, error)
	Get(name string, options metav1.GetOptions) (*authv1.User, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *authv1.User, err error)
	Delete(name string, options *metav1.DeleteOptions) error
	DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error
}

type userClient struct {
	client rest.Interface
}

// Create takes the representation of a user and creates it.  Returns the server's representation of the user, and an error, if there is any.
func (c *userClient) Create(user *authv1.User) (result *authv1.User, err error) {
	result = &authv1.User{}
	err = c.client.Post().
		Resource("users").
		Body(user).
		Do().
		Into(result)
	return
}

func (c *userClient) List(opts metav1.ListOptions) (*authv1.UserList, error) {
	result := authv1.UserList{}
	err := c.client.
		Get().
		Resource("users").
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}

func (c *userClient) Get(name string, opts metav1.GetOptions) (*authv1.User, error) {
	result := authv1.User{}
	err := c.client.
		Get().
		Resource("users").
		Name(name).
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}

// Patch applies the patch and returns the patched user.
func (c *userClient) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *authv1.User, err error) {
	result = &authv1.User{}
	err = c.client.Patch(pt).
		Resource("users").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}

// Delete takes name of the user and deletes it. Returns an error if one occurs.
func (c *userClient) Delete(name string, options *metav1.DeleteOptions) error {
	return c.client.Delete().
		Resource("users").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *userClient) DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error {
	return c.client.Delete().
		Resource("users").
		SpecificallyVersionedParams(&listOptions, scheme.ParameterCodec, versionV1).
		Body(options).
		Do().
		Error()
}

func (c *userClient) Update(user *authv1.User) (result *authv1.User, err error) {
	result = &authv1.User{}
	err = c.client.Put().
		Resource("users").
		Name(user.Name).
		Body(user).
		Do().
		Into(result)
	return
}
