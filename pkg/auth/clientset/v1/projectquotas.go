package v1

import (
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"

	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ProjectQuotaInterface interface {
	List(opts metav1.ListOptions) (*authv1.ProjectQuotaList, error)
	Get(name string, options metav1.GetOptions) (*authv1.ProjectQuota, error)
}

type projectQuotaClient struct {
	client rest.Interface
}

func (c *projectQuotaClient) List(opts metav1.ListOptions) (*authv1.ProjectQuotaList, error) {
	result := authv1.ProjectQuotaList{}
	err := c.client.
		Get().
		Resource("projectquotas").
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}

func (c *projectQuotaClient) Get(name string, opts metav1.GetOptions) (*authv1.ProjectQuota, error) {
	result := authv1.ProjectQuota{}
	err := c.client.
		Get().
		Resource("projectquotas").
		Name(name).
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}
