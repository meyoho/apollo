package v1

import (
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type GroupBindingInterface interface {
	List(opts metav1.ListOptions) (*authv1.GroupBindingList, error)
	Get(name string, options metav1.GetOptions) (*authv1.GroupBinding, error)
}

type groupbindingClient struct {
	client rest.Interface
}

func (c *groupbindingClient) List(opts metav1.ListOptions) (*authv1.GroupBindingList, error) {
	result := authv1.GroupBindingList{}
	err := c.client.
		Get().
		Resource("groupbindings").
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}

func (c *groupbindingClient) Get(name string, opts metav1.GetOptions) (*authv1.GroupBinding, error) {
	result := authv1.GroupBinding{}
	err := c.client.
		Get().
		Resource("groupbindings").
		Name(name).
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}
