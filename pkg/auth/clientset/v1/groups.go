package v1

import (
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type GroupInterface interface {
	List(opts metav1.ListOptions) (*authv1.GroupList, error)
	Get(name string, options metav1.GetOptions) (*authv1.Group, error)
}

type groupClient struct {
	client rest.Interface
}

func (c *groupClient) List(opts metav1.ListOptions) (*authv1.GroupList, error) {
	result := authv1.GroupList{}
	err := c.client.
		Get().
		Resource("groups").
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}

func (c *groupClient) Get(name string, opts metav1.GetOptions) (*authv1.Group, error) {
	result := authv1.Group{}
	err := c.client.
		Get().
		Resource("groups").
		Name(name).
		SpecificallyVersionedParams(&opts, scheme.ParameterCodec, versionV1).
		Do().
		Into(&result)

	return &result, err
}
