package v1

import (
	"log"

	"k8s.io/apimachinery/pkg/runtime/schema"

	"k8s.io/apimachinery/pkg/runtime/serializer"

	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	//clusterv1alpha1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/clusterregistry/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type AuthV1Interface interface {
	Users() UserInterface
	GroupBindings() GroupBindingInterface
	UserBindings() UserBindingInterface
	Projects() ProjectInterface
	ProjectQuotas() ProjectQuotaInterface
	Groups() GroupInterface
	//Clusters(ns string) ClusterInterface
}

type AuthV1Client struct {
	restClient rest.Interface
}

var versionV1 = schema.GroupVersion{Version: "v1"}

var SchemeGroupVersion = authv1.SchemeGroupVersion

func addKnownTypes(scheme *runtime.Scheme) error {
	scheme.AddKnownTypes(SchemeGroupVersion,
		&authv1.User{},
		&authv1.UserList{},
		&authv1.Group{},
		&authv1.GroupList{},
		&authv1.GroupBinding{},
		&authv1.GroupBindingList{},
		&authv1.UserBinding{},
		&authv1.UserBindingList{},
		&authv1.Project{},
		&authv1.ProjectList{},
		&authv1.ProjectBinding{},
		&authv1.ProjectBindingList{},
		&authv1.ProjectQuota{},
		&authv1.ProjectQuotaList{},
		//&clusterv1alpha1.Cluster{},
		//&clusterv1alpha1.ClusterList{},
	)
	metav1.AddToGroupVersion(scheme, SchemeGroupVersion)
	return nil
}

func NewForConfig(c *rest.Config) (*AuthV1Client, error) {

	schemes := runtime.NewScheme()
	SchemeBuilder := runtime.NewSchemeBuilder(addKnownTypes)
	if err := SchemeBuilder.AddToScheme(schemes); err != nil {
		log.Printf("AddToScheme: err: %+v", err)
	}

	config := *c
	config.ContentConfig.GroupVersion = &SchemeGroupVersion
	config.APIPath = "/apis"
	config.AcceptContentTypes = runtime.ContentTypeJSON
	config.ContentType = runtime.ContentTypeJSON
	if config.NegotiatedSerializer == nil {
		config.NegotiatedSerializer = serializer.DirectCodecFactory{CodecFactory: scheme.Codecs}
	}
	if config.UserAgent == "" {
		config.UserAgent = rest.DefaultKubernetesUserAgent()
	}

	client, err := rest.RESTClientFor(&config)
	if err != nil {
		return nil, err
	}

	return &AuthV1Client{restClient: client}, nil
}

func (c *AuthV1Client) Users() UserInterface {
	return &userClient{
		client: c.restClient,
	}
}

func (c *AuthV1Client) GroupBindings() GroupBindingInterface {
	return &groupbindingClient{
		client: c.restClient,
	}
}

func (c *AuthV1Client) UserBindings() UserBindingInterface {
	return &userbindingClient{
		client: c.restClient,
	}
}

func (c *AuthV1Client) Projects() ProjectInterface {
	return &projectClient{
		client: c.restClient,
	}
}

func (c *AuthV1Client) Groups() GroupInterface {
	return &groupClient{
		client: c.restClient,
	}
}

func (c *AuthV1Client) ProjectQuotas() ProjectQuotaInterface {
	return &projectQuotaClient{
		client: c.restClient,
	}
}
