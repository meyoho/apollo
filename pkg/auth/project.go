package auth

import (
	"encoding/json"
	"fmt"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/mathildetech/apollo/api"
	clusterv1alpha1 "bitbucket.org/mathildetech/apollo/api/clusterregistry/v1alpha1"
	authv1client "bitbucket.org/mathildetech/apollo/pkg/auth/clientset/v1"

	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"

	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"

	"github.com/emicklei/go-restful"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

const (
	PLATFORM  = "platform"
	PROJECT   = "project"
	CLUSTER   = "cluster"
	NAMESPACE = "namespace"

	QuotaCPU      = "cpu"
	QuotaMEM      = "memory"
	QuotaStorage  = "storage"
	QuotaPODS     = "pods"
	QuotaPVC      = "persistentvolumeclaims"
	QuotaEStorage = "ephemeral-storage"

	PrefixRequests = "requests"
	PrefixLimits   = "limits"

	// fuzzy search
	DisplayNameProperty = "display-name"
	NameProperty        = "name"
)

var (
	RequestsCPU     = fmt.Sprintf("%s.%s", PrefixRequests, QuotaCPU)
	RequestsMEM     = fmt.Sprintf("%s.%s", PrefixRequests, QuotaMEM)
	RequestsStorage = fmt.Sprintf("%s.%s", PrefixRequests, QuotaStorage)
	LimitsCPU       = fmt.Sprintf("%s.%s", PrefixLimits, QuotaCPU)
	LimitsMEM       = fmt.Sprintf("%s.%s", PrefixLimits, QuotaMEM)
)

var SupportedQuotaTypes = map[corev1.ResourceName]bool{
	corev1.ResourceName(RequestsCPU):     true,
	corev1.ResourceName(LimitsCPU):       true,
	corev1.ResourceName(RequestsMEM):     true,
	corev1.ResourceName(LimitsMEM):       true,
	corev1.ResourceName(RequestsStorage): true,
	corev1.ResourceName(QuotaPODS):       true,
	corev1.ResourceName(QuotaPVC):        true,
}

const ConfigApiTimeout = "apiserver.timeout"
const ConfigApiQps = "apiserver.qps"
const ConfigApiBurst = "apiserver.burst"

// user search relations
type ProjectCell authv1.Project

func (self ProjectCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	if len(self.Annotations) == 0 {
		self.Annotations = map[string]string{}
	}
	switch name {
	case DisplayNameProperty:
		return dataselect.StdComparableContainsString(self.Annotations[constant.AnnotationDisplayName])
	case NameProperty:
		return dataselect.StdComparableContainsString(self.Name)
	default:
		return nil
	}
}

func projectsToCells(std []authv1.Project) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = ProjectCell(std[i])
	}
	return cells
}

func projectFromCells(cells []dataselect.DataCell) []authv1.Project {
	std := make([]authv1.Project, len(cells))
	for i := range std {
		std[i] = authv1.Project(cells[i].(ProjectCell))
	}
	return std
}

type NamespaceCell corev1.Namespace
type FederatedNamespaceCell struct {
	unstructured.Unstructured
}

func (self FederatedNamespaceCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	if len(self.GetAnnotations()) == 0 {
		self.SetAnnotations(map[string]string{})
	}
	switch name {
	case DisplayNameProperty:
		return dataselect.StdComparableContainsString(self.GetAnnotations()[constant.AnnotationDisplayName])
	case NameProperty:
		return dataselect.StdComparableContainsString(self.GetName())
	default:
		return nil
	}
}

func (self NamespaceCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	if len(self.Annotations) == 0 {
		self.Annotations = map[string]string{}
	}
	switch name {
	case DisplayNameProperty:
		return dataselect.StdComparableContainsString(self.Annotations[constant.AnnotationDisplayName])
	case NameProperty:
		return dataselect.StdComparableContainsString(self.Name)
	default:
		return nil
	}
}

func federatedNamespacesToCells(std []unstructured.Unstructured) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = FederatedNamespaceCell{
			std[i],
		}
	}
	return cells
}

func federataedNamespacesFromCells(cells []dataselect.DataCell) []FederatedNamespaceCell {
	std := make([]FederatedNamespaceCell, len(cells))
	for i := range std {
		std[i] = cells[i].(FederatedNamespaceCell)
	}
	return std
}

func namespacesToCells(std []corev1.Namespace) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = NamespaceCell(std[i])
	}
	return cells
}

func namespacesFromCells(cells []dataselect.DataCell) []corev1.Namespace {
	std := make([]corev1.Namespace, len(cells))
	for i := range std {
		std[i] = corev1.Namespace(cells[i].(NamespaceCell))
	}
	return std
}

// end

// HandleProjectList list projects with rbac filtered
func (apiHandler *APIHandler) HandleProjectList(req *restful.Request, res *restful.Response) {
	authClient, err := apiHandler.cManager.AuthV1Client(req)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	availableProjects, err := apiHandler.availableProjects(req)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	logger.Info("HandleListProjects", zap.Any("availableProjects", availableProjects), zap.Any("err", err))

	// fuzzySearch contains query params: ?filterBy=display-name,xxx
	var isFuzzySearch bool
	dataSelect := parseDataSelectPathParameter(req)
	for _, filter := range dataSelect.FilterQuery.FilterByList {
		switch filter.Property {
		case DisplayNameProperty, NameProperty:
			isFuzzySearch = true
			break
		}
	}

	listOpts, err := parseListOpts(req, isFuzzySearch)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	if !Contains(availableProjects, "*") {
		var projects string
		if len(availableProjects) == 0 {
			projects = "()"
		} else {
			projects = fmt.Sprintf("(%s)", strings.Join(availableProjects, ","))
		}
		projectLabel := fmt.Sprintf("%s in %s", constant.LabelProject, projects)
		if listOpts.LabelSelector != "" {
			listOpts.LabelSelector = fmt.Sprintf("%s,%s", listOpts.LabelSelector, projectLabel)
		} else {
			listOpts.LabelSelector = projectLabel
		}
	}

	projectList, err := authClient.Projects().List(*listOpts)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	if isFuzzySearch {
		projectList = fuzzyProjectList(dataSelect, projectList)
	}

	res.WriteHeaderAndEntity(http.StatusOK, projectList)
}

func fuzzyProjectList(dataSelect *dataselect.Query, projectList *authv1.ProjectList) *authv1.ProjectList {
	list := &authv1.ProjectList{
		Items: make([]authv1.Project, 0),
	}

	filterByList := make([]dataselect.FilterBy, 0)
	for _, filter := range dataSelect.FilterQuery.FilterByList {
		switch filter.Property {
		case DisplayNameProperty, NameProperty:
			filterByList = append(filterByList, filter)
		}
	}
	dataSelect.FilterQuery.FilterByList = filterByList

	projectCells, filteredTotal := dataselect.GenericDataSelectWithFilter(projectsToCells(projectList.Items), dataSelect)
	projects := projectFromCells(projectCells)

	// calculate has next page
	nextPage := ""
	if hasNextPage(dataSelect.PaginationQuery.Page, dataSelect.PaginationQuery.ItemsPerPage, filteredTotal) {
		nextPage = strconv.Itoa(dataSelect.PaginationQuery.Page + 2)
	}
	list.ListMeta = metav1.ListMeta{
		Continue: nextPage,
	}

	for _, project := range projects {
		list.Items = append(list.Items, project)
	}

	return list
}

// HandleModules handle modules api
func (apiHandler *APIHandler) HandleProjectNamespaces(req *restful.Request, res *restful.Response) {
	projectName := req.PathParameter("name")
	clusterName := req.PathParameter("cluster-name")

	token, err := ExtractToken(req)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	multipleConfig := rest.Config{
		Host:        apiHandler.GetErebusEndpoint() + "/kubernetes/" + clusterName,
		BearerToken: token,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}

	UpdateConfig(&multipleConfig)

	k8sClient, err := kubernetes.NewForConfig(&multipleConfig)
	if err != nil {
		logger.Error("Init k8s client for multiple clusters", zap.Any("err", err))
		apiHandler.WriteError(res, err)
		return
	}

	projectLabel := fmt.Sprintf("%s=%s", constant.LabelProject, projectName)

	// list namespace under project is always fuzzy
	isFuzzySearch := false
	dataSelect := parseDataSelectPathParameter(req)
	for _, filter := range dataSelect.FilterQuery.FilterByList {
		switch filter.Property {
		case DisplayNameProperty, NameProperty:
			isFuzzySearch = true
			break
		}
	}
	listOpts, err := parseListOpts(req, isFuzzySearch)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	if listOpts.LabelSelector != "" {
		listOpts.LabelSelector = fmt.Sprintf("%s,%s", listOpts.LabelSelector, projectLabel)
	} else {
		listOpts.LabelSelector = projectLabel
	}

	namespaces, err := k8sClient.CoreV1().Namespaces().List(*listOpts)
	namespaces.TypeMeta = metav1.TypeMeta{
		Kind:       "NamespaceList",
		APIVersion: corev1.SchemeGroupVersion.String(),
	}
	if err != nil {
		logger.Error("List namespace with multiple clusters", zap.Any("err", err))
		apiHandler.WriteError(res, err)
		return
	}

	if !apiHandler.cManager.IsSecureModeEnabled(req) {
		res.WriteHeaderAndEntity(http.StatusOK, namespaces)
		return
	}

	rbacNamespace, err := ResourceFilter(apiHandler.cManager, NewFilterQuery(nsFilter(projectName, clusterName)), req)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	// if Contains(rbacNamespace, "*") {
	// 	res.WriteHeaderAndEntity(http.StatusOK, namespaces)
	// 	return
	// }

	namespaceList := new(corev1.NamespaceList)
	namespaceList.TypeMeta = namespaces.TypeMeta
	namespaceList.ListMeta = namespaces.ListMeta
	namespaceList.Items = make([]corev1.Namespace, 0)

	for _, namespace := range namespaces.Items {
		if Contains(rbacNamespace, namespace.Name) {
			namespaceList.Items = append(namespaceList.Items, namespace)
		}
	}

	if isFuzzySearch {
		namespaceList = fuzzyNamespaceList(dataSelect, namespaceList)
	}

	res.WriteHeaderAndEntity(http.StatusOK, namespaceList)
}

// HandleModules handle modules api
func (apiHandler *APIHandler) HandleProjectFederatedNamespaces(req *restful.Request, res *restful.Response) {
	projectName := req.PathParameter("name")
	clusterName := req.PathParameter("cluster-name")

	token, err := ExtractToken(req)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	multipleConfig := rest.Config{
		Host:        apiHandler.GetErebusEndpoint() + "/kubernetes/" + clusterName,
		BearerToken: token,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}

	UpdateConfig(&multipleConfig)

	k8sClient, err := dynamic.NewForConfig(&multipleConfig)
	if err != nil {
		logger.Error("Init k8s client for multiple clusters", zap.Any("err", err))
		apiHandler.WriteError(res, err)
		return
	}

	projectLabel := fmt.Sprintf("%s=%s", constant.LabelProject, projectName)

	// list namespace under project is always fuzzy
	isFuzzySearch := false
	dataSelect := parseDataSelectPathParameter(req)
	for _, filter := range dataSelect.FilterQuery.FilterByList {
		switch filter.Property {
		case DisplayNameProperty, NameProperty:
			isFuzzySearch = true
			break
		}
	}
	//logger.Info("dataSelect is ", zap.Any("dataSelect", dataSelect),zap.Any("isFuzzySearch", isFuzzySearch))
	listOpts, err := parseListOpts(req, isFuzzySearch)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	if listOpts.LabelSelector != "" {
		listOpts.LabelSelector = fmt.Sprintf("%s,%s", listOpts.LabelSelector, projectLabel)
	} else {
		listOpts.LabelSelector = projectLabel
	}
	gvk := schema.GroupVersionResource{
		Group:    "types.kubefed.io",
		Version:  "v1beta1",
		Resource: "federatednamespaces",
	}

	namespaces, err := k8sClient.Resource(gvk).List(*listOpts)
	//logger.Info("federatedns", zap.Any("namespaces", namespaces))
	if err != nil {
		logger.Error("List namespace with multiple clusters", zap.Any("err", err))
		apiHandler.WriteError(res, err)
		return
	}

	if !apiHandler.cManager.IsSecureModeEnabled(req) {
		res.WriteHeaderAndEntity(http.StatusOK, namespaces)
		return
	}

	rbacNamespace, err := ResourceFilter(apiHandler.cManager, NewFilterQuery(nsFilter(projectName, clusterName)), req)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	namespaceList := &unstructured.UnstructuredList{}
	namespaceList.SetAPIVersion("types.kubefed.io/v1beta1")
	namespaceList.SetKind("FederatedNamespaceList")
	namespaceList.SetContinue(namespaces.GetContinue())
	namespaceList.SetResourceVersion(namespaces.GetResourceVersion())
	namespaceList.SetSelfLink(namespaces.GetSelfLink())
	namespaceList.Items = make([]unstructured.Unstructured, 0)

	for _, namespace := range namespaces.Items {
		if Contains(rbacNamespace, namespace.GetName()) {
			namespaceList.Items = append(namespaceList.Items, namespace)
		}
	}
	if isFuzzySearch {
		namespaceList = fuzzyFederatedNamespaceList(dataSelect, namespaceList)
	}

	res.WriteHeaderAndEntity(http.StatusOK, namespaceList)
}

func fuzzyFederatedNamespaceList(dataSelect *dataselect.Query, federatedNamespaceList *unstructured.UnstructuredList) *unstructured.UnstructuredList {
	namespaceList := &unstructured.UnstructuredList{}
	namespaceList.SetAPIVersion("types.kubefed.io/v1beta1")
	namespaceList.SetKind("FederatedNamespaceList")
	namespaceList.SetContinue(federatedNamespaceList.GetContinue())
	namespaceList.SetResourceVersion(federatedNamespaceList.GetResourceVersion())
	namespaceList.SetSelfLink(federatedNamespaceList.GetSelfLink())
	namespaceList.Items = make([]unstructured.Unstructured, 0)

	filterByList := make([]dataselect.FilterBy, 0)
	for _, filter := range dataSelect.FilterQuery.FilterByList {
		switch filter.Property {
		case DisplayNameProperty, NameProperty:
			filterByList = append(filterByList, filter)
		}
	}
	dataSelect.FilterQuery.FilterByList = filterByList

	namespaceCells, filteredTotal := dataselect.GenericDataSelectWithFilter(federatedNamespacesToCells(federatedNamespaceList.Items), dataSelect)
	namespaces := federataedNamespacesFromCells(namespaceCells)

	// calculate has next page
	nextPage := ""
	if hasNextPage(dataSelect.PaginationQuery.Page, dataSelect.PaginationQuery.ItemsPerPage, filteredTotal) {
		nextPage = strconv.Itoa(dataSelect.PaginationQuery.Page + 2)
	}
	namespaceList.SetContinue(nextPage)

	for _, namespace := range namespaces {
		namespaceList.Items = append(namespaceList.Items, namespace.Unstructured)
	}

	return namespaceList
}

func fuzzyNamespaceList(dataSelect *dataselect.Query, namespaceList *corev1.NamespaceList) *corev1.NamespaceList {
	list := new(corev1.NamespaceList)
	list.TypeMeta = namespaceList.TypeMeta
	list.ListMeta = namespaceList.ListMeta
	list.Items = make([]corev1.Namespace, 0)

	filterByList := make([]dataselect.FilterBy, 0)
	for _, filter := range dataSelect.FilterQuery.FilterByList {
		switch filter.Property {
		case DisplayNameProperty, NameProperty:
			filterByList = append(filterByList, filter)
		}
	}
	dataSelect.FilterQuery.FilterByList = filterByList

	namespaceCells, filteredTotal := dataselect.GenericDataSelectWithFilter(namespacesToCells(namespaceList.Items), dataSelect)
	namespaces := namespacesFromCells(namespaceCells)

	// calculate has next page
	nextPage := ""
	if hasNextPage(dataSelect.PaginationQuery.Page, dataSelect.PaginationQuery.ItemsPerPage, filteredTotal) {
		nextPage = strconv.Itoa(dataSelect.PaginationQuery.Page + 2)
	}
	list.ListMeta.Continue = nextPage

	for _, namespace := range namespaces {
		list.Items = append(list.Items, namespace)
	}

	return list
}

func (apiHandler *APIHandler) HandleProjectResources(req *restful.Request, res *restful.Response) {
	clusterClient, err := apiHandler.cManager.ClusterV1Alpha1Client(req)
	if err != nil {
		logger.Error("Failed to init clusterClient", zap.Any("err", err))
		apiHandler.WriteError(res, err)
		return
	}

	namespace := GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	listOpts := metav1.ListOptions{}

	// list all clusters crd
	clusters, err := clusterClient.Clusters(namespace).List(listOpts)
	if err != nil {
		logger.Error("Failed to list all clusters in system", zap.Any("err", err))
		apiHandler.WriteError(res, err)
		return
	}

	// extract the token of request user for request remote cluster
	token, err := ExtractToken(req)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	// result
	clusterQuotas := authv1.ClusterQuotaList{}

	for _, cluster := range clusters.Items {
		clusterQuota := authv1.ClusterQuota{
			ObjectMeta: metav1.ObjectMeta{
				Name: cluster.Name,
			},
		}
		multipleConfig := rest.Config{
			Host:        apiHandler.GetErebusEndpoint() + "/kubernetes/" + cluster.Name,
			BearerToken: token,
			TLSClientConfig: rest.TLSClientConfig{
				Insecure: true,
			},
		}
		UpdateConfig(&multipleConfig)

		// construct the k8s client to list all nodes
		k8sClient, err := kubernetes.NewForConfig(&multipleConfig)
		if err != nil {
			logger.Error("Init k8s client for multiple clusters", zap.Any("err", err), zap.Any("cluster", cluster.Name))
			continue
		}

		// list hosts in cluster
		hosts, err := k8sClient.CoreV1().Nodes().List(listOpts)
		if err != nil {
			logger.Error("List nodes with multiple clusters", zap.Any("err", err), zap.Any("cluster", cluster.Name))
			continue
		}

		for _, host := range hosts.Items {
			// clusterQuota.Status.Capacity is sum of node.status.Allocatable
			clusterQuota.Status.Capacity = QuotaAdd(clusterQuota.Status.Capacity, formatResourceList(host.Status.Allocatable))
		}
		// init used with zero quantity
		clusterQuota.Status.Used = QuotaSubtract(clusterQuota.Status.Capacity, clusterQuota.Status.Capacity)

		// construct the auth client to list all project quota
		authV1Client, err := authv1client.NewForConfig(&multipleConfig)
		if err != nil {
			logger.Error("Init auth client for multiple clusters", zap.Any("err", err), zap.Any("cluster", cluster.Name))
			continue
		}

		// list project quota in cluster
		quotas, err := authV1Client.ProjectQuotas().List(listOpts)
		if err != nil {
			logger.Error("List projectquota with multiple clusters", zap.Any("err", err), zap.Any("cluster", cluster.Name))
			continue
		}

		for _, quota := range quotas.Items {
			// clusterQuota.Status.Used is sum of Project.Spec.Hard
			clusterQuota.Status.Used = QuotaAdd(clusterQuota.Status.Used, formatResourceList(quota.Spec.Hard))
		}
		// clusterQuota.Status.Allocatable is (clusterQuota.Status.Capacity-clusterQuota.Status.Used)
		clusterQuota.Status.Allocatable = QuotaSubtract(clusterQuota.Status.Capacity, formatResourceList(clusterQuota.Status.Used))

		clusterQuotas.Items = append(clusterQuotas.Items, clusterQuota)

	}

	res.WriteHeaderAndEntity(http.StatusOK, clusterQuotas)
}

func formatResourceName(source corev1.ResourceName) []corev1.ResourceName {
	/*
		ephemeral-storage  -> requests.storage
		cpu                -> requests.cpu and limits.cpu
		memory             -> requests.memory and limits.memory
	*/
	result := make([]corev1.ResourceName, 0)
	switch string(source) {
	case QuotaEStorage:
		result = append(result, corev1.ResourceName(RequestsStorage))
	case QuotaCPU:
		result = append(result, corev1.ResourceName(RequestsCPU))
		result = append(result, corev1.ResourceName(LimitsCPU))
	case QuotaMEM:
		result = append(result, corev1.ResourceName(RequestsMEM))
		result = append(result, corev1.ResourceName(LimitsMEM))
	default:
		result = append(result, source)
	}
	return result
}

func formatResourceList(source corev1.ResourceList) corev1.ResourceList {
	result := corev1.ResourceList{}

	for key, value := range source {
		quantity := *value.Copy()
		resourceNames := formatResourceName(key)
		for _, resourceName := range resourceNames {
			if _, supported := SupportedQuotaTypes[resourceName]; supported {
				result[resourceName] = quantity
			}
		}
	}

	return result
}

// QuotaAdd and QuotaSubtract are copied from k8s.io/kubernetes/pkg/quota/v1 for avoid go mod bugs?
// QuotaAdd returns the result of a + b for each named resource
func QuotaAdd(a corev1.ResourceList, b corev1.ResourceList) corev1.ResourceList {
	result := corev1.ResourceList{}
	for key, value := range a {
		quantity := *value.Copy()
		if other, found := b[key]; found {
			quantity.Add(other)
		}
		result[key] = quantity
	}
	for key, value := range b {
		if _, found := result[key]; !found {
			quantity := *value.Copy()
			result[key] = quantity
		}
	}
	return result
}

// Subtract returns the result of a - b for each named resource
func QuotaSubtract(a corev1.ResourceList, b corev1.ResourceList) corev1.ResourceList {
	result := corev1.ResourceList{}
	for key, value := range a {
		quantity := *value.Copy()
		if other, found := b[key]; found {
			quantity.Sub(other)
		}
		result[key] = quantity
	}
	for key, value := range b {
		if _, found := result[key]; !found {
			quantity := *value.Copy()
			quantity.Neg()
			result[key] = quantity
		}
	}
	return result
}

// GetEnv get value from os environment, if not available, then return fallback.
func GetEnv(key, fallback string) string {
	value, exists := os.LookupEnv(key)
	if exists && len(value) != 0 {
		return value
	}
	return fallback
}

// GetCluster construct a dynamic client and request cluster crd
func GetCluster(dynamicClient dynamic.Interface, clusterName string) (*clusterv1alpha1.Cluster, error) {
	clusterClient := dynamicClient.Resource(schema.GroupVersionResource{
		Group:    "clusterregistry.k8s.io",
		Version:  "v1alpha1",
		Resource: "clusters",
	})

	ns := GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	obj, err := clusterClient.Namespace(ns).Get(clusterName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	cluster := clusterv1alpha1.Cluster{}
	b, err := json.Marshal(obj.Object)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(b, &cluster)
	if err != nil {
		return nil, err
	}

	return &cluster, nil
}

// Extracts authorization information from request header
func ExtractToken(req *restful.Request) (string, error) {
	if req == nil {
		return "", nil
	}

	authHeader := req.HeaderParameter("Authorization")

	// Authorization header will be more important than our token
	token := ExtractTokenFromHeader(authHeader)
	if len(token) > 0 {
		return token, nil
	}

	return "", nil
}

// ExtractTokenFromHeader get the Authorization value and trim the Bearer
func ExtractTokenFromHeader(authHeader string) string {
	if strings.HasPrefix(authHeader, "Bearer ") {
		return strings.TrimPrefix(authHeader, "Bearer ")
	}

	return ""
}

// Contains check if target exist in slice
func Contains(slice []string, target string) bool {
	for _, item := range slice {
		if item == target || item == "*" {
			return true
		}
	}

	return false
}

// nsFilter return an rawString for construct QueryFilter
// get the available namespaces under project with projectName
func nsFilter(projectName, clusterName string) []string {
	rawString := []string{PROJECT, projectName, CLUSTER, clusterName}
	return rawString
}

func (apiHandler *APIHandler) availableProjects(request *restful.Request) ([]string, error) {

	if !apiHandler.cManager.IsSecureModeEnabled(request) {
		return []string{"*"}, nil
	}
	jwtToken, err := api.ParseJWTFromHeader(request)
	if err != nil {
		return nil, err
	}

	authClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		return nil, err
	}
	email := EmailToName(jwtToken.Email)
	userbindings, err := authClient.UserBindings().List(metav1.ListOptions{
		LabelSelector: fmt.Sprintf("%s=%s", constant.LabelUserEmail, email),
	})
	if err != nil {
		return []string{}, err
	}

	projects := []string{}
	for _, binding := range userbindings.Items {
		roleLevel := binding.Labels[constant.LabelRoleLevel]
		// platform level is *
		if roleLevel == PLATFORM {
			return []string{"*"}, nil
		} else if roleLevel == PROJECT || roleLevel == NAMESPACE {
			if project, ok := binding.Labels[constant.LabelProject]; ok {
				if !Contains(projects, project) {
					projects = append(projects, project)
				}
			}
		}
	}

	return projects, nil
}

func (apiHandler *APIHandler) GetErebusEndpoint() string {
	erebusNamespace := GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	erebusEndpoint := fmt.Sprintf("%s.%s.%s", "https://erebus", erebusNamespace, "svc.cluster.local")
	return GetEnv("EREBUS_ENDPOINT", erebusEndpoint)
}

func UpdateConfig(config *rest.Config) {
	logger.Info("UpdateConfig", zap.Any("api-timeout", viper.GetInt(ConfigApiTimeout)), zap.Any("api-qps", viper.GetInt(ConfigApiQps)), zap.Any("api-burst", viper.GetInt(ConfigApiBurst)))
	// default timeout to 2s
	config.Timeout = time.Duration(viper.GetInt(ConfigApiTimeout)) * time.Second
	// default qps to 100
	config.QPS = float32(viper.GetInt(ConfigApiQps))
	// default burst to 100
	config.Burst = viper.GetInt(ConfigApiBurst)
}
