package auth

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"

	ers "errors"
	"github.com/emicklei/go-restful"

	"bitbucket.org/mathildetech/apollo/api"
	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func (apiHandler *APIHandler) HandleCreateUserBinding(request *restful.Request, response *restful.Response) {
	userBinding := new(authv1.UserBinding)
	if err := request.ReadEntity(userBinding); err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	if err := userBinding.Validate(); err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	// check request header auth info
	token, err := api.ParseJWTFromHeader(request)
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	if userBinding.UserEmailName() == EmailToName(token.Email) {
		err := ers.New("can't create userbindings for yourself")
		apiHandler.WriteError(response, errors.NewForbidden(schema.GroupResource{
			rc.GroupName,
			"userbindings",
		}, "", err))
		return
	}

	// valid userbinding email format
	userEmail := strings.TrimSpace(userBinding.UserEmail())
	if len(userEmail) > 0 && EmailToName(userEmail) != userBinding.UserEmailName() {
		apiHandler.WriteError(response, fmt.Errorf("email invalid"))
		return
	}

	crdClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}
	crdClientInsecure, err := apiHandler.cManager.InsecureAuthV1Client()
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	// can't create userbindings for admin
	userName := userBinding.UserEmailName()
	user, err := crdClientInsecure.Users().Get(userName, metav1.GetOptions{})
	if err != nil {
		if ! errors.IsNotFound(err) {
			apiHandler.WriteError(response, err)
			return
		}
	} else if user.Spec.IsAdmin {
		err := ers.New("can't create userbindings for admin user")
		apiHandler.WriteError(response, errors.NewForbidden(schema.GroupResource{
			rc.GroupName,
			"userbindings",
		}, "", err))
		return
	}

	// check if the user exists
	if _, err = crdClient.Users().Get(userBinding.UserEmailName(), metav1.GetOptions{}); err != nil {
		if errors.IsNotFound(err) && len(userEmail) > 0 {
			// register an unknown user
			if err := apiHandler.RegisterUnknownUser(userEmail); err != nil && !errors.IsAlreadyExists(err) {
				apiHandler.WriteError(response, err)
				return
			}
		} else {
			apiHandler.WriteError(response, err)
			return
		}
	}

	// check if the clusterrole exists
	//k8sClient, err := apiHandler.cManager.Client(request)
	//if err != nil {
	//	apiHandler.WriteError(response, err)
	//	return
	//}
	//if _, err = k8sClient.RbacV1().ClusterRoles().Get(userBinding.RoleName(), metav1.GetOptions{}); err != nil {
	//	apiHandler.WriteError(response, err)
	//	return
	//}

	// if it wasn't the administrator
	if !token.Ext.IsAdmin {
		resource := schema.GroupResource{
			Group:    rc.GroupName,
			Resource: "userbindings",
		}
		constraints := Newconstraints(userBinding)
		userEmailName := EmailToName(token.Email)
		// verify that the current user has permission to assign roles
		verify, err := apiHandler.Verify(request, userEmailName, "create", resource, constraints)
		if err != nil {
			apiHandler.WriteError(response, err)
			return
		}

		if !verify {
			statusErr := &errors.StatusError{metav1.Status{
				Status:  metav1.StatusFailure,
				Code:    http.StatusForbidden,
				Reason:  metav1.StatusReasonForbidden,
				Message: "Not enough permissions",
			}}
			apiHandler.WriteError(response, statusErr)
			return
		}
	}

	if _, err := crdClient.UserBindings().Create(userBinding); err != nil {
		if errors.IsAlreadyExists(err) {
			apiHandler.WriteError(response, fmt.Errorf("This role for user \"%s\" already exists.", userBinding.UserEmail()))
		} else {
			apiHandler.WriteError(response, err)
		}
		return
	}
	response.WriteHeaderAndEntity(http.StatusCreated, userBinding)
}

func (apiHandler *APIHandler) HandleDeleteUserBinding(request *restful.Request, response *restful.Response) {
	token, err := api.ParseJWTFromHeader(request)
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	crdClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}
	name := request.PathParameter("name")
	userBinding, err := crdClient.UserBindings().Get(name, metav1.GetOptions{})
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}

	crdClientInsecure, err := apiHandler.cManager.InsecureAuthV1Client()
	if err != nil {
		apiHandler.WriteError(response, err)
		return
	}
	// can't delete userbindings for admin
	userName := userBinding.UserEmailName()
	user, err := crdClientInsecure.Users().Get(userName, metav1.GetOptions{})
	if err != nil {
		if !errors.IsNotFound(err) {
			apiHandler.WriteError(response, err)
			return
		}
	} else if user.Spec.IsAdmin {
		err := ers.New("can't delete userbindings for admin user")
		apiHandler.WriteError(response, errors.NewForbidden(schema.GroupResource{
			rc.GroupName,
			"userbindings",
		}, "", err))
		return
	}

	if userBinding.UserEmailName() == EmailToName(token.Email) {
		err := ers.New("can't delete userbindings for yourself")
		apiHandler.WriteError(response, errors.NewForbidden(schema.GroupResource{
			rc.GroupName,
			"userbindings",
		}, "", err))
		return
	}

	if !token.Ext.IsAdmin {
		userEmailName := EmailToName(token.Email)
		resource := schema.GroupResource{
			Group:    rc.GroupName,
			Resource: "userbindings",
		}
		constraints := Newconstraints(userBinding)
		verify, err := apiHandler.Verify(request, userEmailName, "delete", resource, constraints)
		if err != nil {
			apiHandler.WriteError(response, err)
			return
		}
		if !verify {
			statusErr := &errors.StatusError{metav1.Status{
				Status:  metav1.StatusFailure,
				Code:    http.StatusForbidden,
				Reason:  metav1.StatusReasonForbidden,
				Message: "Not enough permissions",
			}}
			apiHandler.WriteError(response, statusErr)
			return
		}
	}
	if err := crdClient.UserBindings().Delete(userBinding.Name, &metav1.DeleteOptions{}); err != nil {
		apiHandler.WriteError(response, err)
		return
	}
	response.WriteHeader(http.StatusOK)
}

func Newconstraints(userBinding *authv1.UserBinding) map[string]string {
	constraints := map[string]string{}
	if len(userBinding.ProjectName()) > 0 {
		constraints[ResProject] = userBinding.ProjectName()
	}
	if len(userBinding.NamespaceName()) > 0 {
		constraints[ResNamespace] = userBinding.NamespaceName()
	}
	if len(userBinding.NamespaceCluster()) > 0 {
		constraints[ResCluster] = userBinding.NamespaceCluster()
	}
	return constraints
}

func (apiHandler *APIHandler) RegisterUnknownUser(email string) error {
	crdClient, err := apiHandler.cManager.InsecureAuthV1Client()
	if err != nil {
		return err
	}
	emailName := EmailToName(email)
	user := authv1.User{
		TypeMeta: metav1.TypeMeta{
			Kind:       "User",
			APIVersion: authv1.SchemeGroupVersion.String(),
		},
		ObjectMeta: metav1.ObjectMeta{
			Labels: map[string]string{
				constant.LabelUserConnectorID:   "",
				constant.LabelUserConnectorType: "",
				constant.LabelUserEmail:         emailName,
				constant.LabelUserUsername:      "",
				constant.LabelUserValid:         "true",
			},
			Annotations: map[string]string{
				constant.AnnotationDisplayName: "",
			},
			Name: emailName,
		},
		Spec: authv1.UserSpec{
			ConnectorName: "",
			ConnectorType: "",
			Email:         email,
			Groups:        []string{ungrouped},
			IsAdmin:       false,
			Username:      "",
			Valid:         true,
		},
	}
	if _, err := crdClient.Users().Create(&user); err != nil && !errors.IsAlreadyExists(err) {
		return err
	}
	return nil
}

func validateEmail(email string) bool {
	Re := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	return Re.MatchString(email)
}

func EmailToName(email string) string {
	if len(email) == 0 {
		return ""
	}
	name := email
	name = util.GetMD5Hash(name)
	name = strings.ToLower(name)
	return name
}
