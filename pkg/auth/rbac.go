package auth

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/mathildetech/apollo/api"
	clientapi "bitbucket.org/mathildetech/apollo/client/api"
	authclientapi "bitbucket.org/mathildetech/apollo/pkg/auth/clientset/v1"
	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	restful "github.com/emicklei/go-restful"
	"go.uber.org/zap"
	authorizationv1 "k8s.io/api/authorization/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

const (
	ResProject      = "res:project"
	ResNamespace    = "res:ns"
	ResResourceName = "res:name"
	ResCluster      = "res:cluster"
)

type Permission struct {
	RoleName    string
	Actions     []string
	Constraints map[string]string
	Resource    schema.GroupResource
}

type SelfSubjectAccessReview struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty" protobuf:"bytes,1,opt,name=metadata"`
	Spec              SelfSubjectAccessReviewSpec               `json:"spec" protobuf:"bytes,2,opt,name=spec"`
	Status            authorizationv1.SubjectAccessReviewStatus `json:"status,omitempty" protobuf:"bytes,3,opt,name=status"`
}

type SelfSubjectAccessReviewSpec struct {
	ResourceAttributes *ResourceAttributes `json:"resourceAttributes,omitempty" protobuf:"bytes,1,opt,name=resourceAttributes"`
}

type ResourceAttributes struct {
	Project   string `json:"project,omitempty"`
	Namespace string `json:"namespace,omitempty" protobuf:"bytes,1,opt,name=namespace"`
	Cluster   string `json:"cluster,omitempty"`
	Verb      string `json:"verb,omitempty" protobuf:"bytes,2,opt,name=verb"`
	Group     string `json:"group,omitempty" protobuf:"bytes,3,opt,name=group"`
	Resource  string `json:"resource,omitempty" protobuf:"bytes,5,opt,name=resource"`
	Name      string `json:"name,omitempty" protobuf:"bytes,7,opt,name=name"`
}

func (apiHandler *APIHandler) handleGetResourceFilter(request *restful.Request, res *restful.Response) {
	filterQuery := parseFilterPathParameter(request)
	logger.Info("filterQuery", zap.Any("filter", filterQuery))

	resourcesList, err := ResourceFilter(apiHandler.cManager, filterQuery, request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	res.WriteHeaderAndEntity(http.StatusOK, newResourceList(resourcesList))
}

func ResourceFilter(mgr clientapi.ClientManager, query *FilterQuery, request *restful.Request) ([]string, error) {
	return simpleFilter(mgr, query, request)
}

func simpleFilter(mgr clientapi.ClientManager, query *FilterQuery, request *restful.Request) ([]string, error) {

	if !mgr.IsSecureModeEnabled(request) {
		return []string{"*"}, nil
	}
	jwtToken, err := api.ParseJWTFromHeader(request)
	if err != nil {
		return nil, err
	}

	if jwtToken.Ext.IsAdmin {
		return []string{"*"}, nil
	}

	// get user userbindings
	var (
		projectName string
		clusterName string
	)

	for _, filter := range query.FilterByList {
		if filter.Property == "project" {
			projectName = filter.Value
		}
		if filter.Property == "cluster" {
			clusterName = filter.Value
		}
	}

	authClient, err := mgr.AuthV1Client(request)
	if err != nil {
		return nil, err
	}
	email := EmailToName(jwtToken.Email)
	userbindings, err := authClient.UserBindings().List(metav1.ListOptions{
		LabelSelector: constant.LabelUserEmail + "=" + email,
	})

	if err != nil {
		return nil, err
	}

	// key -> []{resourcenames...}
	var namespacesMap = make(map[string][]string, 0)
	for _, binding := range userbindings.Items {
		roleLevel := binding.Labels[constant.LabelRoleLevel]
		// platform level is *
		if roleLevel == "platform" {
			namespacesMap = make(map[string][]string, 0)
			namespacesMap["*"] = []string{"*"}
			break
		}

		// userbinding for project
		if project, ok := binding.Labels[constant.LabelProject]; ok && project == projectName {
			// project level is *
			if roleLevel == "project" {
				namespacesMap = make(map[string][]string, 0)
				namespacesMap[project] = []string{"*"}
				break
			}

			// loop ns res
			if roleLevel == "namespace" {
				ns := binding.Labels[constant.LabelNamespace]
				if util.StringInSlice(ns, namespacesMap[project]) {
					continue
				}
				if len(namespacesMap[project]) == 0 {
					namespacesMap[project] = []string{}
				}

				if len(clusterName) > 0 && binding.Labels[constant.LabelCluster] != clusterName {
					continue
				}
				namespacesMap[project] = append(namespacesMap[project], ns)
			}
		}
	}

	resList := make([]string, 0)
	for project, resources := range namespacesMap {
		if project == "*" || project == projectName {
			resList = resources
		}
	}

	return resList, nil
}

func newResourceList(res []string) *api.ResourcesList {
	return &api.ResourcesList{
		TypeMeta: metav1.TypeMeta{
			Kind:       "ResourcesList",
			APIVersion: "v1",
		},
		Items: res,
	}
}

func (apiHandler *APIHandler) Verify(request *restful.Request, user string, action string, resource schema.GroupResource, constraints map[string]string) (bool, error) {
	actions, err := apiHandler.GetActions(request, user, resource, constraints, true)
	logger.Info("rbac.Verify", zap.Any("user", user), zap.Any("resource", resource), zap.Any("constraints", constraints), zap.Any("action", action), zap.Any("actions", actions))
	if err != nil {
		return false, err
	}
	return hasAction(action, actions), nil
}

func (apiHandler *APIHandler) GetActions(request *restful.Request, user string, resource schema.GroupResource, constraints map[string]string, useCache bool) ([]string, error) {
	if constraints == nil {
		constraints = map[string]string{}
	}

	if user == "" {
		return nil, errors.NewBadRequest("user is empty")
	}

	if resource.Resource == "" {
		return nil, errors.NewBadRequest("resource is empty")
	}
	if useCache {

	}

	userPerms, err := apiHandler.GetUserPermissions(request, user, resource)
	if err != nil {
		return nil, err
	}

	actions := apiHandler.GetActionsForResourceFast(userPerms, constraints)
	return actions, nil
}

func (apiHandler *APIHandler) GetUserPermissions(request *restful.Request, user string, resource schema.GroupResource) ([]*Permission, error) {
	crdClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		return nil, err
	}
	userbindingList, err := GetUserBindingList(crdClient, user)
	if err != nil {
		return nil, err
	}

	permissions := make([]*Permission, 0)
	if userbindingList == nil {
		return permissions, nil
	}

	for _, userbinding := range userbindingList.Items {
		ch := make(chan []*Permission)
		go func(chnl chan []*Permission) {
			perms, err := apiHandler.GetUserBindingPermissions(request, &userbinding, resource)
			if err != nil {
				logger.Error("GetUserBindingPermissions", zap.Any("resource", resource), zap.Any("userbinding", userbinding))
			}
			chnl <- perms
			close(chnl)
		}(ch)
		for perms := range ch {
			permissions = append(permissions, perms...)
		}
	}

	return permissions, nil
}

func (apiHandler *APIHandler) GetUserBindingPermissions(request *restful.Request, userbinding *authv1.UserBinding, resource schema.GroupResource) ([]*Permission, error) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		return nil, err
	}
	labelSelector := fmt.Sprintf("%s,%s=%s", constant.LabelRoleRelative, constant.LabelRoleRelative, userbinding.RoleName())
	opts := metav1.ListOptions{
		LabelSelector: labelSelector,
	}
	clusterRoleList, err := k8sClient.RbacV1().ClusterRoles().List(opts)
	if err != nil {
		return nil, err
	}
	permissions := make([]*Permission, 0)
	for _, clusterRole := range clusterRoleList.Items {
		ch := make(chan *Permission)
		go func(clusterRole *rbacv1.ClusterRole, userbinding *authv1.UserBinding, resource schema.GroupResource, chnl chan *Permission) {
			for _, rule := range clusterRole.Rules {
				// loop apigroup
				for _, group := range rule.APIGroups {

					if resource.Group != group && group != "*" {
						continue
					}

					// loop resources
					for _, res := range rule.Resources {

						if resource.Resource != res && res != "*" {
							continue
						}

						// loop resourceNames
						if len(rule.ResourceNames) > 0 {
							for _, resourceName := range rule.ResourceNames {

								chnl <- NewPermission(userbinding, resource, rule.Verbs, resourceName)
							}
						} else {
							chnl <- NewPermission(userbinding, resource, rule.Verbs, "")
						}

					} // end resources

				} // end apigroup
			}
			close(chnl)

		}(&clusterRole, userbinding, resource, ch)

		for perm := range ch {
			permissions = append(permissions, perm)
		}
	}
	return permissions, nil
}

func (apiHandler *APIHandler) GetActionsForResourceFast(permissions []*Permission, constraints map[string]string) []string {
	var (
		actionsMap = make(map[string]struct{})
		place      = struct{}{}
		actions    []string
	)

	for _, p := range permissions {
		log.Printf("GetActionsForResourceFast.permission: %+v", *p)
		if p.IsInConstraint(constraints) {
			for _, a := range p.Actions {
				actionsMap[a] = place
			}
		}
	}

	actions = make([]string, 0)
	for k := range actionsMap {
		if k == "*" {
			actions = []string{"*"}
			break
		}
		actions = append(actions, k)
	}
	return actions
}

func (apiHandler *APIHandler) HandleCreateSelfSubjectAccessReview(request *restful.Request, res *restful.Response) {
	accessReview := &SelfSubjectAccessReview{}
	if err := request.ReadEntity(accessReview); err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	token, err := api.ParseJWTFromHeader(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	verify := false
	//if !token.Ext.IsAdmin {
	userEmailName := EmailToName(token.Email)
	resource := schema.GroupResource{
		Group:    accessReview.Spec.ResourceAttributes.Group,
		Resource: accessReview.Spec.ResourceAttributes.Resource,
	}
	constraints := map[string]string{}
	if len(accessReview.Spec.ResourceAttributes.Project) > 0 {
		constraints[ResProject] = accessReview.Spec.ResourceAttributes.Project
	}
	if len(accessReview.Spec.ResourceAttributes.Namespace) > 0 {
		constraints[ResNamespace] = accessReview.Spec.ResourceAttributes.Namespace
	}
	if len(accessReview.Spec.ResourceAttributes.Cluster) > 0 {
		constraints[ResCluster] = accessReview.Spec.ResourceAttributes.Cluster
	}
	if len(accessReview.Spec.ResourceAttributes.Name) > 0 {
		constraints[ResResourceName] = accessReview.Spec.ResourceAttributes.Name
	}
	verify, err = apiHandler.Verify(request, userEmailName, accessReview.Spec.ResourceAttributes.Verb, resource, constraints)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	//} else {
	//	verify = true
	//}

	accessReview.Status = authorizationv1.SubjectAccessReviewStatus{
		Allowed: verify,
	}

	res.WriteHeaderAndEntity(http.StatusCreated, accessReview)
}

func NewPermission(userbinding *authv1.UserBinding, resource schema.GroupResource, actions []string, resourceName string) *Permission {
	constraints := make(map[string]string)
	if len(userbinding.ProjectName()) > 0 {
		constraints[ResProject] = userbinding.ProjectName()
	}
	if len(userbinding.NamespaceName()) > 0 {
		constraints[ResNamespace] = userbinding.NamespaceName()
	}
	if len(resourceName) > 0 {
		constraints[ResResourceName] = resourceName
	}
	if len(userbinding.NamespaceCluster()) > 0 {
		constraints[ResCluster] = userbinding.NamespaceCluster()
	}

	return &Permission{
		RoleName:    userbinding.RoleName(),
		Actions:     actions,
		Constraints: constraints,
		Resource:    resource,
	}
}

func GetUserBindingList(cli authclientapi.AuthV1Interface, email string) (list *authv1.UserBindingList, err error) {
	labeSelector := fmt.Sprintf("%s,%s=%s", constant.LabelUserEmail, constant.LabelUserEmail, email)
	list, err = cli.UserBindings().List(metav1.ListOptions{
		LabelSelector: labeSelector,
	})
	if err != nil {
		return nil, err
	}
	return
}

// hasAction - verifies if the string is in the slice
// action: action string e.g service:create
// allowed: slice of allowed actions e.g. service:create, service:update, etc..
func hasAction(action string, allowed []string) bool {
	for _, a := range allowed {
		if a == "*" {
			return true
		}
		if a == action {
			return true
		}
	}
	return false
}

// IsInConstraint returns true if the permission constrains is equal or broader than the given
func (p *Permission) IsInConstraint(constraints map[string]string) bool {
	// no constrains, allows all
	if len(p.Constraints) == 0 {
		return true
	}
	if p.matchAllConstraints(constraints) {
		return true
	}

	return false
}

func (p *Permission) matchAllConstraints(constraints map[string]string) bool {
	//log.Printf("************p.Constraints: %+v, constraints: %+v", p.Constraints, constraints)

	for k, v := range p.Constraints {
		if val, ok := constraints[k]; !ok || v != val {
			//log.Printf(">>>>>>>constraints, k: %+v , val: %+v, v: %+v", k, val, v)
			return false
		}
	}
	return true
}
