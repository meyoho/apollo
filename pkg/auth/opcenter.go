package auth

import (
	"encoding/json"
	restful "github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"net/http"
	"sort"
)

var LabelProductInstallable = "%s/installable=true"
var LabelProductIsDisplay = "%s/isdisplay"

type ProductItem struct {
	ProductCrd *unstructured.Unstructured  `json:"crd"`
	ProductCr  []unstructured.Unstructured `json:"crs"`
	Error      error                       `json:"error"`
}

type ProductItemSlice []ProductItem

func (p ProductItemSlice) Len() int { return len(p) }

func (p ProductItemSlice) Less(i, j int) bool {
	return p[i].ProductCrd.GetName() > p[j].ProductCrd.GetName()
}

func (p ProductItemSlice) Swap(i, j int) { p[i], p[j] = p[j], p[i] }

type ProductList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ProductItem `json:"items"`
}

var (
	crdGVR = schema.GroupVersionResource{
		Group:    "apiextensions.k8s.io",
		Version:  "v1beta1",
		Resource: "customresourcedefinitions",
	}
)

type NameSpec struct {
	Kind   string `json:"kind"`
	Plural string `json:"plural"`
}

type CustomDefineSpec struct {
	Group   string `json:"group"`
	Scope   string `json:"scope"`
	Version string `json:"version"`

	Names NameSpec `json:"names"`
}

type CustomDefine struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec CustomDefineSpec `json:"spec,omitempty"`
}

func GetcrdFromUnstructured(r *unstructured.Unstructured) (*CustomDefine, error) {
	b, err := json.Marshal(r.Object)
	if err != nil {
		return nil, err
	}
	var a CustomDefine
	if err := json.Unmarshal(b, &a); err != nil {
		return nil, err
	}
	return &a, nil
}

func (apiHandler *APIHandler) getProductCrdCrs(dynClient dynamic.Interface, prcrd unstructured.Unstructured, ch chan *ProductItem) {

	var pritem ProductItem
	pritem.ProductCrd = &prcrd
	crd, err := GetcrdFromUnstructured(&prcrd)
	if err != nil {
		pritem.Error = err
		ch <- &pritem
	}

	crGVR := schema.GroupVersionResource{
		Group:    crd.Spec.Group,
		Version:  crd.Spec.Version,
		Resource: crd.Spec.Names.Plural,
	}
	crClient := dynClient.Resource(crGVR)
	crs, err := crClient.List(metav1.ListOptions{})
	if err != nil {
		pritem.Error = err
		ch <- &pritem
	}
	var crlist []unstructured.Unstructured
	if crs != nil {
		if len(crs.Items) > 0 {
			for _, critem := range crs.Items {
				crlist = append(crlist, critem)
			}
		}
	}
	pritem.ProductCr = crlist
	ch <- &pritem
}

func (apiHandler *APIHandler) handleGetProducts(request *restful.Request, res *restful.Response) {
	hasCr := request.QueryParameter("hascr")

	dynClient, err := apiHandler.cManager.DynamicClient(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	crdClient := dynClient.Resource(crdGVR)
	crds, err := crdClient.List(metav1.ListOptions{
		//change to constant
		LabelSelector: LabelProductInstallable,
	})
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	var products []ProductItem
	ch := make(chan *ProductItem, len(crds.Items))
	for _, item := range crds.Items {
		go apiHandler.getProductCrdCrs(dynClient, item, ch)
	}

	for i := 0; i < len(crds.Items); i++ {
		//wait for all crd get crs return
		prditem := <-ch
		//if we do not have any cr, and query param have cr, then not add
		if hasCr == "true" && (len(prditem.ProductCr) == 0) {
			continue
		}
		products = append(products, *prditem)
	}

	sortProducts := apiHandler.SortProducts(products)
	prlist := ProductList{
		Items: sortProducts,
		ListMeta: metav1.ListMeta{
			ResourceVersion: crds.GetResourceVersion(),
			Continue:        crds.GetContinue(),
		},
		TypeMeta: metav1.TypeMeta{
			Kind: "ProductList",
		},
	}

	res.WriteHeaderAndEntity(http.StatusOK, prlist)
}

func (apiHandler *APIHandler) SortProducts(products ProductItemSlice) ProductItemSlice {
	var acpproducts ProductItemSlice
	var otherproducts ProductItemSlice
	var allproducts ProductItemSlice
	for _, item := range products {
		if _, ok := item.ProductCrd.GetLabels()[LabelProductIsDisplay]; ok {
			acpproducts = append(acpproducts, item)
		} else {
			otherproducts = append(otherproducts, item)
		}
	}
	sort.Sort(acpproducts)
	sort.Sort(otherproducts)
	for _, aitem := range acpproducts {
		allproducts = append(allproducts, aitem)
	}
	for _, oitem := range otherproducts {
		allproducts = append(allproducts, oitem)
	}

	return allproducts
}

func (apiHandler *APIHandler) handleGetProduct(request *restful.Request, res *restful.Response) {
	crdName := request.PathParameter("name")

	dynClient, err := apiHandler.cManager.DynamicClient(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	crdClient := dynClient.Resource(crdGVR)
	crd, err := crdClient.Get(crdName, metav1.GetOptions{})
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	ch := make(chan *ProductItem)
	go apiHandler.getProductCrdCrs(dynClient, *crd, ch)

	prditem := <-ch

	res.WriteHeaderAndEntity(http.StatusOK, prditem)
}
