package auth

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"reflect"
	"strings"
	"time"

	clientapi "bitbucket.org/mathildetech/apollo/client/api"
	authclientapi "bitbucket.org/mathildetech/apollo/pkg/auth/clientset/v1"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/log"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"

	"github.com/btcsuite/btcutil/base58"
	"github.com/emicklei/go-restful"
	"github.com/ghodss/yaml"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

const (
	TypeLDAP  = "ldap"
	TypeLOCAL = "local"
	TypeOIDC  = "oidc"
	TypeALL   = "all"

	ConfigSyncLdapTimeout = "sync.ldap-timeout"
)

var dexLogger log.Logger

// handleSyncLdap will sync the user between crd and ldap
func (apiHandler *APIHandler) handleSyncUsers(request *restful.Request, res *restful.Response) {
	config, err := getDexConfig(apiHandler.cManager, request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	dexLogger, _ = log.NewLogger(config.Logger.Level, config.Logger.Format)

	typeStr := request.QueryParameter("type")
	if typeStr == "" {
		apiHandler.WriteError(res, errors.New("type is required"))
		return
	}

	logger.Info("sync users from idp", zap.Any("type", typeStr))

	// construct authclient
	authClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	// get users from k8s
	dataSelect := parseDataSelectPathParameter(request)
	crdUsers, err := getUsers(apiHandler.cManager, dataSelect, request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	var failed, success, total int

	switch typeStr {
	case TypeLDAP:
		users, ldapErr := apiHandler.handleGetLdapUsers(config)
		if ldapErr != nil {
			err = ldapErr
			break
		}
		total = len(users)
		failed, err = syncUsers(TypeLDAP, authClient, crdUsers, users)
	case TypeLOCAL:
		users , localErr := apiHandler.handleGetLocalUsers(config)
		if localErr != nil {
			err = localErr
			break
		}
		total = len(users)
		failed, localErr = syncUsers(TypeLOCAL, authClient, crdUsers, users)
		if localErr != nil {
			err = localErr
			break
		}
	case TypeALL:
		var localFailed int
		localUsers, localErr := apiHandler.handleGetLocalUsers(config)
		if localErr != nil {
			err = localErr
			break
		}
		localFailed, localErr = syncUsers(TypeLOCAL, authClient, crdUsers, localUsers)
		if localErr != nil {
			err = localErr
			break
		}
		ldapUsers, ldapErr := apiHandler.handleGetLdapUsers(config)
		if ldapErr != nil {
			err = ldapErr
			break
		}
		var ldapFailed int
		ldapFailed, err = syncUsers(TypeLDAP, authClient, crdUsers, ldapUsers)
		if err != nil {
			break
		}
		total = len(ldapUsers) + len(localUsers)
		failed = ldapFailed + localFailed
	}
	success = total - failed

	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	stastics := map[string]int{
		"total":   total,
		"success": success,
		"fail":    failed,
	}

	detailMsg, _ := json.Marshal(stastics)

	successStatus := metav1.Status{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Status",
			APIVersion: "v1",
		},
		Status:  metav1.StatusSuccess,
		Code:    int32(http.StatusOK),
		Reason:  metav1.StatusReasonUnknown,
		Message: "Successfully synced users",
		Details: &metav1.StatusDetails{
			Causes: []metav1.StatusCause{
				{
					Message: string(detailMsg),
				},
			},
		},
	}

	res.WriteHeaderAndEntity(http.StatusOK, successStatus)
}

func (apiHandler *APIHandler) handleGetLocalUsers(config *idp.Config) ([]authv1.User, error) {
	users := make([]authv1.User, 0)

	storageClient, err := config.Storage.Config.Open(dexLogger)
	if err != nil {
		return nil, err
	}

	passwords, err :=  storageClient.ListPasswords()
	if err != nil {
		return nil, err
	}
	for _, password := range passwords {
		groups := []string{}
		user := constructAuthUser(password.Email, password.Username, TypeLOCAL, TypeLOCAL, TypeLOCAL, groups, password.Account, password.IsAdmin)
		users = append(users, user)
	}

	return users, nil
}

// handleGetLdapUsers will sync the user between crd and ldap
func (apiHandler *APIHandler) handleGetLdapUsers(config *idp.Config) ([]authv1.User, error) {
	allUsers := make([]authv1.User, 0)
	var err error

	storageClient, _ := config.Storage.Config.Open(dexLogger)
	storageConnectors, _ := storageClient.ListConnectors()

	connectors := []idp.Connector{}
	for _, sconnector := range storageConnectors {
		connector, _ := idp.FromStorageConnector(sconnector)
		connectors = append(connectors, *connector)
	}
	connectors = append(connectors, config.StaticConnectors...)

	for _, connector := range connectors {
		switch connector.Type {
		case TypeLDAP:
			users, ldapErr := getLDAPUsers(connector)
			if ldapErr != nil {
				logger.Error("err when query ldap", zap.Any("name", connector.Name), zap.Any("id", connector.ID), zap.Any("err", err))
				err = ldapErr
				break
			}
			allUsers = append(allUsers, users...)
		default:
			continue
		}
	}

	logger.Info("User from ldap", zap.Any("users", allUsers), zap.Any("error", err))

	return allUsers, err
}

// getDexConfig query k8s for dex-configmap
func getDexConfig(mgr clientapi.ClientManager, request *restful.Request) (*idp.Config, error) {
	k8sClient, err := mgr.Client(request)
	if err != nil {
		return nil, err
	}

	dexConfigNs := GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	dexConfigName := GetEnv("DEX_CONFIGMAP", "dex-configmap")
	dexConfigKey := GetEnv("DEX_KEY", "config.yaml")

	cm, err := k8sClient.CoreV1().ConfigMaps(dexConfigNs).Get(dexConfigName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	var config idp.Config

	strConfig := cm.Data[dexConfigKey]

	if strConfig == "" {
		return nil, errors.New("Invalid dex configmap")
	}

	err = yaml.Unmarshal([]byte(strConfig), &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}

// getLDAPUsers get all available identities from specified connector
func getLDAPUsers(ldapConnector idp.Connector) (users []authv1.User, err error) {
	logger.Info("getLdapIdentities", zap.Any("type", ldapConnector.Type), zap.Any("name", ldapConnector.Name), zap.Any("id", ldapConnector.ID))
	ldapConfig := ldapConnector.Config
	client, err := ldapConfig.Open(ldapConnector.ID, dexLogger)
	identitiesConnector := client.(connector.IdentitiesConnector)
	if err != nil {
		logger.Error("err when connect to ldap", zap.Any("name", ldapConnector.Name), zap.Any("id", ldapConnector.ID), zap.Any("err", err))
		return
	}

	scope := connector.Scopes{
		OfflineAccess: true,
		Groups:        true,
	}

	identities := []connector.Identity{}
	c := make(chan int, 1)

	go func() {
		identities, err = identitiesConnector.Identities(context.TODO(), scope)
		c <- 1
	}()

	syncLdapTimeout := viper.GetInt(ConfigSyncLdapTimeout)

	select {
	case <-c:
		break
	case <-time.After(time.Duration(syncLdapTimeout) * time.Second):
		err = errors.New("Timeout when retrieve identities from ldap server")
		logger.Error("err when query ldap", zap.Any("name", ldapConnector.Name), zap.Any("id", ldapConnector.ID), zap.Any("err", err))
		break
	}

	if err != nil {
		return
	}

	for _, ident := range identities {
		groups := []string{ungrouped}
		if len(ident.Groups) > 0 {
			groups = ident.Groups
		}

		user := constructAuthUser(ident.Email, ident.Username, ldapConnector.Type, ldapConnector.Name, ldapConnector.ID, groups, "", false)
		users = append(users, user)
	}

	return users, nil
}

func constructAuthUser(email, username, connectorType, connectorName, connectorId string, groups []string, account string, isAdmin bool ) authv1.User {
	// all false now
	if len(groups) == 0 {
		groups = append(groups, ungrouped)
	}

	return authv1.User{
		TypeMeta: metav1.TypeMeta{
			Kind:       "User",
			APIVersion: "auth.alauda.io/v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: EmailToName(email),
			Labels: map[string]string{
				constant.LabelUserEmail:         EmailToName(email),
				constant.LabelUserUsername:      "", //EncodeBase58(username),
				constant.LabelUserValid:         "true",
				constant.LabelUserConnectorType: connectorType,
				constant.LabelUserConnectorID:   connectorId,
			},
			Annotations: map[string]string{
				constant.AnnotationDisplayName: username,
			},
		},
		Spec: authv1.UserSpec{
			ConnectorType: connectorType,
			ConnectorName: connectorName,
			Email:         email,
			Username:      username,
			Groups:        groups,
			Valid:         true,
			IsAdmin:       isAdmin,
			Account:       account,
		},
	}

}

func syncUsers(connectorType string, authClient authclientapi.AuthV1Interface, crdUsers *authv1.UserList, currentUsers []authv1.User) (failed int, err error) {
	logger.Info("syncUsers", zap.Any("connector type", connectorType), zap.Any("total users", len(currentUsers)))
	currentUsersMapper := make(map[string]authv1.User)
	for _, currentUser := range currentUsers {
		currentUsersMapper[currentUser.Name] = currentUser
	}

	validCrdUsersMapper := make(map[string]authv1.User)
	invalidCrdUsersMapper := make(map[string]authv1.User)
	crdUsersMapper := make(map[string]authv1.User)

	// check validity of current crd users
	for _, crdUser := range crdUsers.Items {
		crdUsersMapper[crdUser.Name] = crdUser
		if crdUser.Spec.ConnectorType != connectorType {
			continue
		}
		if _, exist := currentUsersMapper[crdUser.Name]; exist {
			validCrdUsersMapper[crdUser.Name] = crdUser
		} else {
			invalidCrdUsersMapper[crdUser.Name] = crdUser
		}
	}

	// check not exist ldap users
	needAddLdapUsersMapper := make(map[string]authv1.User)
	for _, currentUser := range currentUsers {
		// don't sync ldap user that has same name of admin local user
		if connectorType != TypeLOCAL {
			if localUser, exist := crdUsersMapper[currentUser.Name]; exist {
				if IsAdminLocalUser(localUser) {
					continue
				}
			}
		}
		if _, exist := validCrdUsersMapper[currentUser.Name]; !exist {
			needAddLdapUsersMapper[currentUser.Name] = currentUser
		}
	}

	for name, crdUser := range validCrdUsersMapper {
		currentUser, _ := currentUsersMapper[name]

		changed := false
		// update spec
		if !reflect.DeepEqual(crdUser.Spec, currentUser.Spec) {
			changed = true
			crdUser.Spec.Email = currentUser.Spec.Email
			crdUser.Spec.Groups = currentUser.Spec.Groups
			crdUser.Spec.Valid = currentUser.Spec.Valid
			crdUser.Spec.IsAdmin = currentUser.Spec.IsAdmin
			crdUser.Spec.ConnectorType = currentUser.Spec.ConnectorType
			crdUser.Spec.ConnectorName = currentUser.Spec.ConnectorName
			crdUser.Spec.Account = currentUser.Spec.Account
		}

		// update labels
		if len(crdUser.Labels) == 0 {
			crdUser.Labels = map[string]string{}
		}
		if !reflect.DeepEqual(crdUser.Labels, currentUser.Labels) {
			changed = true
			crdUser.Labels[constant.LabelUserEmail] = currentUser.Labels[constant.LabelUserEmail]
			crdUser.Labels[constant.LabelUserUsername] = currentUser.Labels[constant.LabelUserUsername]
			crdUser.Labels[constant.LabelUserValid] = currentUser.Labels[constant.LabelUserValid]
			crdUser.Labels[constant.LabelUserConnectorType] = currentUser.Labels[constant.LabelUserConnectorType]
			crdUser.Labels[constant.LabelUserConnectorID] = currentUser.Labels[constant.LabelUserConnectorID]
		}

		if len(crdUser.Annotations) == 0 {
			crdUser.Annotations = map[string]string{}
		}
		if crdUser.Annotations[constant.AnnotationDisplayName] != currentUser.Annotations[constant.AnnotationDisplayName] {
			changed = true
			crdUser.Annotations[constant.AnnotationDisplayName] = currentUser.Annotations[constant.AnnotationDisplayName]
		}

		if strings.ToLower(crdUser.Labels[constant.LabelUserValid]) != "true" {
			changed = true
			crdUser.Labels[constant.LabelUserValid] = "true"
		}

		if !changed {
			continue
		}
		crdUser.Annotations[constant.AnnotationCreator] = currentUser.Annotations[constant.AnnotationCreator]

		_, err = authClient.Users().Update(&crdUser)
		if err != nil {
			logger.Error("Update auth user error", zap.Any("auth user", crdUser), zap.Any("ldap user", currentUser), zap.Any("err", err))
			failed++
		}
	}

	for name, crdUser := range invalidCrdUsersMapper {
		crdUser.Spec.Valid = false
		crdUser.Labels[constant.LabelUserValid] = "false"
		data, _ := json.Marshal(crdUser)

		_, err = authClient.Users().Patch(name, types.MergePatchType, data)
		if err != nil {
			logger.Error("Mark auth user as invalid error", zap.Any("auth user", crdUser), zap.Any("err", err))
			failed++
		}
	}

	for _, currentUser := range needAddLdapUsersMapper {
		_, err = authClient.Users().Create(&currentUser)
		if k8serrors.IsAlreadyExists(err) {
			oldUser, err := authClient.Users().Get(currentUser.Name, metav1.GetOptions{})
			if err != nil {
				logger.Error("Create auth user but user with same name exist", zap.Any("err", err))
				continue
			}
			oldUser.TypeMeta = currentUser.TypeMeta
			oldUser.Spec.Email = currentUser.Spec.Email
			oldUser.Spec.Groups = currentUser.Spec.Groups
			oldUser.Spec.Valid = currentUser.Spec.Valid
			oldUser.Spec.IsAdmin = currentUser.Spec.IsAdmin
			oldUser.Spec.ConnectorType = currentUser.Spec.ConnectorType
			oldUser.Spec.ConnectorName = currentUser.Spec.ConnectorName
			oldUser.Spec.Account = currentUser.Spec.Account
			oldUser.Labels[constant.LabelUserValid] = currentUser.Labels[constant.LabelUserValid]
			oldUser.Labels[constant.LabelUserEmail] = currentUser.Labels[constant.LabelUserEmail]
			oldUser.Labels[constant.LabelUserUsername] = currentUser.Labels[constant.LabelUserUsername]
			oldUser.Labels[constant.LabelUserValid] = currentUser.Labels[constant.LabelUserValid]
			oldUser.Labels[constant.LabelUserConnectorType] = currentUser.Labels[constant.LabelUserConnectorType]
			oldUser.Labels[constant.LabelUserConnectorID] = currentUser.Labels[constant.LabelUserConnectorID]
			oldUser.Annotations[constant.AnnotationDisplayName] = currentUser.Annotations[constant.AnnotationDisplayName]

			_, err = authClient.Users().Update(oldUser)
			if err != nil {
				logger.Error("Update exist users", zap.Any("auth user", oldUser), zap.Any("err", err))
				failed++
			} else {
				continue
			}
		} else if err != nil {
			logger.Error("Create auth user", zap.Any("user", currentUser), zap.Any("err", err))
			failed++
		}
	}
	logger.Info("syncUsers", zap.Any("connector type", connectorType), zap.Any("total users", len(currentUsers)), zap.Any("failed users", failed))

	return failed, nil
}

func EncodeBase58(source string) string {
	return base58.Encode([]byte(source))
}

func IsAdminLocalUser(user authv1.User) bool {
	 if user.Spec.IsAdmin && user.Spec.ConnectorType == TypeLOCAL {
	 	return true
	 }
	 return false
}
