package auth

import (
	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	er "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/btcsuite/btcutil/base58"

	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"

	"github.com/emicklei/go-restful"

	"bitbucket.org/mathildetech/apollo/api"
	clientapi "bitbucket.org/mathildetech/apollo/client/api"
	authclientapi "bitbucket.org/mathildetech/apollo/pkg/auth/clientset/v1"
	authclientv1beta1 "bitbucket.org/mathildetech/apollo/pkg/auth/clientset/v1beta1"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	authv1beta1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1beta1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	dexapi "bitbucket.org/mathildetech/dex/api"
	"go.uber.org/zap"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

const (
	ungrouped   = "ungrouped"
	ungroupedCN = "未分组"

	UsernameProperty  = "username"
	EmailProperty     = "email"
	GroupProperty     = "group"
	ProjectProperty   = "project"
	NamespaceProperty = "namespace"
	RoleNameProperty  = "role_name"
	RoleLevelProperty = "role_level"
	ClusterProperty   = "cluster"
	NotinProperty     = "notin"

	LocalConnector = "local"

	emailPattern        = "^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$"
	accountPattern      = `^[a-z]([-.a-z\d]*[a-z\d])?(\.[a-z\d]([-a-z\d]*[a-z\d])?)*$`
	passwordOnlyDigit   = `^\d+$`
	passwordOnlyChar    = `^[a-zA-Z]+$`
	passwordOnlySpecial = `^[!@#$%&*()]+$`
	passwordPattern     = `^[\da-zA-Z!@#$%&*()]{6,20}$`
)

// user search relations
type UserCell authv1.User

type UserList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []authv1.User `json:"items"`
}

func (self UserCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case UsernameProperty:
		return dataselect.StdComparableContainsString(self.Spec.Username)
	case EmailProperty:
		return dataselect.StdComparableContainsString(self.Spec.Email)
	default:
		return nil
	}
}

func toCells(std []authv1.User) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = UserCell(std[i])
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []authv1.User {
	std := make([]authv1.User, len(cells))
	for i := range std {
		std[i] = authv1.User(cells[i].(UserCell))
	}
	return std
}

// end

func (apiHandler *APIHandler) handleGetUserList(request *restful.Request, res *restful.Response) {

	dataSelect := parseDataSelectPathParameter(request)
	logger.Info("dataSelect", zap.Any("dataSelect", dataSelect))

	list, err := getUsers(apiHandler.cManager, dataSelect, request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	// TODO optimize Kind
	if list != nil {
		list.Kind = "UserList"
	}

	res.WriteHeaderAndEntity(http.StatusOK, list)
}

func (apiHandler *APIHandler) HandleCreateUser(request *restful.Request, res *restful.Response) {

	// check permissions
	if !canIActionUser(apiHandler, request, "create") {
		res.WriteHeader(http.StatusForbidden)
		return
	}

	localUser := new(authv1.User)
	if err := ReadUserEntity(request, localUser); err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	err := CheckUserSpec(localUser)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	authClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	if isExist, err := IsNotLocalUserExists(authClient, localUser.Spec.Email); err != nil || isExist {
		if err != nil {
			apiHandler.WriteError(res, err)
			return
		}
		if isExist {
			apiHandler.WriteError(res, errors.New(
				fmt.Sprintf("user %s is exists", localUser.Spec.Email)))
			return
		}
	}

	createReq, err := BuildCreatePasswordReqFromUser(*localUser)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	cli, err := newDexClient()
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	resp, err := cli.CreatePassword(context.TODO(), createReq)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	if resp.AlreadyExists {
		apiHandler.WriteError(res, errors.New(fmt.Sprintf(
			"Users '%s' is already exists", localUser.Spec.Email)))
		return
	}

	FillUser(localUser)

	localUser, err = authClient.Users().Create(localUser)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	res.WriteHeaderAndEntity(http.StatusCreated, localUser)
}

func IsNotLocalUserExists(authClient authclientapi.AuthV1Interface, email string) (bool, error) {
	email = strings.ToLower(email)
	name := util.GetMD5Hash(email)
	user, err := authClient.Users().Get(name, metav1.GetOptions{})
	if err != nil {
		if er.IsNotFound(err) {
			return false, nil
		}
		return true, err
	}
	if user.Spec.ConnectorType != TypeLOCAL {
		return true, nil
	}
	return false, nil
}

func ReadUserEntity(request *restful.Request, user *authv1.User) error {
	err := request.ReadEntity(user)
	return err
}

func FillUser(user *authv1.User) {
	user.Spec.Password = ""
	user.Spec.OldPassword = ""
	user.Spec.ConnectorType = LocalConnector
	user.Spec.ConnectorName = LocalConnector
	user.Spec.IsAdmin = false
	user.Spec.Valid = true
	base58Email := string(EmailToName(user.Spec.Email))
	user.Name = base58Email
	user.Annotations = map[string]string{
		rc.AnnotationDisplayName: user.Spec.Username,
	}
	user.Labels = map[string]string{
		rc.LabelUserConnectorID:   LocalConnector,
		rc.LabelUserConnectorType: LocalConnector,
		rc.LabelUserEmail:         base58Email,
		rc.LabelUserValid:         "true",
		rc.LabelUserUsername:      "",
	}
}

func BuildCreatePasswordReqFromUser(user authv1.User) (*dexapi.CreatePasswordReq, error) {
	req := new(dexapi.CreatePasswordReq)

	plainPassword, err := base64.StdEncoding.DecodeString(user.Spec.Password)
	if err != nil {
		return req, err
	}

	encryptedPassword, err := bcrypt.GenerateFromPassword([]byte(plainPassword), bcrypt.DefaultCost)
	if err != nil {
		return req, err
	}

	return &dexapi.CreatePasswordReq{
		Password: &dexapi.Password{
			Email:    user.Spec.Email,
			Hash:     encryptedPassword,
			Username: user.Spec.Username,
			UserId:   uuid.New().String(),
			Account:  user.Spec.Account,
		},
	}, nil
}

func CheckUserSpec(user *authv1.User) error {
	if user.Spec.Email == "" {
		return errors.New("user email can't be empty")
	}
	if user.Spec.Account == "" {
		return errors.New("account can't be empty")
	}
	if len(user.Spec.Account) > 253 {
		return errors.New(fmt.Sprintf("account length mast less than %d", 253))
	}

	matched, err := regexp.MatchString(accountPattern, user.Spec.Account)
	if err != nil {
		return err
	}
	if !matched {
		return errors.New(fmt.Sprintf("account need satify regex '%s'", accountPattern))
	}

	matched, err = regexp.MatchString(emailPattern, user.Spec.Email)
	if err != nil {
		return err
	}
	if !matched {
		return errors.New(fmt.Sprintf("email need satify regex '%s'", emailPattern))
	}

	plainPassword, err := base64.StdEncoding.DecodeString(user.Spec.Password)
	if err != nil {
		return err
	}

	err = CheckPassword(plainPassword)
	if err != nil {
		return err
	}
	return nil
}

func CheckPassword(password []byte) error {
	matched, err := regexp.Match(passwordOnlyDigit, password)
	if err != nil {
		return err
	}
	if matched {
		return errors.New("password don't only contains digit")
	}
	matched, err = regexp.Match(passwordOnlyChar, password)
	if err != nil {
		return err
	}
	if matched {
		return errors.New("password don't only contains character")
	}
	matched, err = regexp.Match(passwordOnlySpecial, password)
	if err != nil {
		return err
	}
	if matched {
		return errors.New("password don't only contains special character")
	}
	matched, err = regexp.Match(passwordPattern, password)
	if err != nil {
		return err
	}
	if !matched {
		return errors.New(fmt.Sprintf("password need match '%s'", passwordPattern))
	}
	return nil
}

func canIActionUser(handler *APIHandler, request *restful.Request, action string) bool {

	selfSubjectAccessReview := clientapi.ToSelfSubjectAccessReview("", "", "users", "auth.alauda.io", action)

	return handler.cManager.CanI(request, selfSubjectAccessReview)
}

func (apiHandler *APIHandler) HandlePatchUser(request *restful.Request, res *restful.Response) {

	requestBody, err := ioutil.ReadAll(request.Request.Body)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	pathBody := make(map[string]interface{})
	err = json.Unmarshal(requestBody, &pathBody)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	var userPassword interface{}
	var userOldPassword interface{}
	var username interface{}
	if spec, ok1 := pathBody["spec"]; ok1 {
		if userSpec, ok2 := spec.(map[string]interface{}); ok2 {
			if acc, ok := userSpec["username"]; ok {
				username = acc
			}else {
				username = nil
			}
			if pass, ok := userSpec["password"]; ok {
				userPassword = pass
			}else {
				userPassword = nil
			}
			if oldPass, ok := userSpec["old_password"]; ok {
				userOldPassword = oldPass
			}else {
				userOldPassword = nil
			}
		}
	}

	isSelf, err := CheckRequestIsSelf(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	// not user can't reset self password
	if isSelf && IsResetPasswords(userPassword, userOldPassword) {
		res.WriteHeader(http.StatusForbidden)
		return
	}

	if !isSelf && !canIActionUser(apiHandler, request, "update") {
		res.WriteHeader(http.StatusForbidden)
		return
	}

	authClient, err := apiHandler.cManager.InsecureAuthV1Client()
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	jwtToken, err := api.ParseJWTFromHeader(request)
	if err != nil {
		apiHandler.WriteError(res, err)
	}
	name := EmailToName(jwtToken.Email)
	_, err = authClient.Users().Get(name, metav1.GetOptions{})
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	userName := request.PathParameter("name")

	user, err := authClient.Users().Get(userName, metav1.GetOptions{})
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	if !isSelf && user.Spec.IsAdmin {
		res.WriteHeader(http.StatusForbidden)
		return
	}

	cli, err := newDexClient()
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	// List all passwords.
	resp, err := cli.ListPasswords(context.TODO(), &dexapi.ListPasswordReq{})
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	password := new(dexapi.Password)
	for _, pass := range resp.Passwords {
		if pass.Email == user.Spec.Email {
			password = pass
			break
		}
	}
	if password.Email == "" {
		apiHandler.WriteError(res, errors.New("no such local users exist"))
		return
	}

	updateReq, err := GetUpdatePasswordReq(password, userPassword, userOldPassword, username)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	_, err = cli.UpdatePassword(context.TODO(), updateReq)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	if username != nil {
		user.Spec.Username = username.(string)
		user.Annotations[rc.AnnotationDisplayName] = username.(string)
		data, err := json.Marshal(user)
		if err != nil {
			apiHandler.WriteError(res, err)
			return
		}
		_, err = authClient.Users().Patch(userName, types.MergePatchType, data)
		if err != nil {
			apiHandler.WriteError(res, err)
			return
		}
	}
	res.WriteHeader(http.StatusOK)
}

func IsResetPasswords(password, oldPassword interface{}) bool {
	if oldPassword == nil && password != nil {
		return true
	}
	return false
}

func GetUpdatePasswordReq(password *dexapi.Password, userPassword, userOldPassword, username interface{}) (*dexapi.UpdatePasswordReq, error) {
	var plainOldPassword, plainPassword, encryptedPassword []byte
	var err error

	updateReq := &dexapi.UpdatePasswordReq{
		Email: password.Email,
	}

	// update password
	if userOldPassword != nil {

		plainOldPassword, err = base64.StdEncoding.DecodeString(userOldPassword.(string))
		if err != nil {
			return nil, err
		}
		err = bcrypt.CompareHashAndPassword(password.Hash, plainOldPassword)
		if err != nil {
			if err == bcrypt.ErrMismatchedHashAndPassword {
				return nil, errors.New("the old password is not correct")
			}
			return nil, err
		}
	}

	if userPassword != nil {
		plainPassword, err = base64.StdEncoding.DecodeString(userPassword.(string))
		if err != nil {
			return nil, err
		}
		err = CheckPassword(plainPassword)
		if err != nil {
			return nil, err
		}
		encryptedPassword, err = bcrypt.GenerateFromPassword([]byte(plainPassword), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}
		updateReq.NewHash = encryptedPassword
	}

	if username != nil {
		updateReq.NewUsername = username.(string)
	}else {
		updateReq.NewUsername = password.Username
	}

	return updateReq, nil
}

func CheckRequestIsSelf(request *restful.Request) (bool, error) {
	jwtToken, err := api.ParseJWTFromHeader(request)
	if err != nil {
		return false, err
	}

	name := request.PathParameter("name")
	if name == EmailToName(jwtToken.Email) {
		return true, nil
	}
	return false, nil
}

func (apiHandler *APIHandler) HandleDeleteUser(request *restful.Request, res *restful.Response) {

	isSelf, err := CheckRequestIsSelf(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	if isSelf {
		res.WriteHeader(http.StatusForbidden)
		return
	}

	if !canIActionUser(apiHandler, request, "delete") {
		res.WriteHeader(http.StatusForbidden)
		return
	}
	userName := request.PathParameter("name")

	authClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	user, err := authClient.Users().Get(userName, metav1.GetOptions{})
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	if user.Spec.ConnectorType == TypeLOCAL && user.Spec.Valid {

		cli, err := newDexClient()
		if err != nil {
			apiHandler.WriteError(res, err)
			return
		}

		deleteReq := &dexapi.DeletePasswordReq{
			Email: user.Spec.Email,
		}

		_, err = cli.DeletePassword(context.TODO(), deleteReq)
		if err != nil {
			apiHandler.WriteError(res, err)
			return
		}
	}

	err = authClient.Users().Delete(userName, &metav1.DeleteOptions{})
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	res.WriteHeader(http.StatusOK)
}

type UserRole struct {
	Name  string   `json:"name"`
	Roles []string `json:"roles"`
}

type UserRoleList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []UserRole `json:"items"`
}

func (apiHandler *APIHandler) HandleGetUsersRoles(request *restful.Request, res *restful.Response) {
	authClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	queryUsers := request.QueryParameter("users")
	labelSelector := fmt.Sprintf("%s,%s in (%s)", rc.LabelUserEmail, rc.LabelUserEmail, queryUsers)
	userBindingList, err := authClient.UserBindings().List(metav1.ListOptions{
		LabelSelector: labelSelector,
	})
	userRoleList := new(UserRoleList)
	userRoles := make(map[string][]string)
	for _, userName := range strings.Split(queryUsers, ",") {
		userRoles[userName] = []string{}
	}
	for _, userBinding := range userBindingList.Items {
		name, ok1 := userBinding.Labels[rc.LabelUserEmail]
		roleName, ok2 := userBinding.Labels[rc.LabelRoleName]
		if ok1 && ok2 {
			userRoles[name] = append(userRoles[name], roleName)
		}
	}
	for key, value := range (userRoles) {
		userRoleList.Items = append(userRoleList.Items, UserRole{Name: key, Roles: value,})
	}
	userRoleList.Kind = "UserRoleList"
	userRoleList.APIVersion = "auth.alauda.io/v1"
	res.WriteHeaderAndEntity(http.StatusOK, userRoleList)
}

func getUsers(mgr clientapi.ClientManager, dataSelect *dataselect.Query, request *restful.Request) (*authv1.UserList, error) {
	var (
		userbindingLabels = make([]string, 0)

		// conditions
		isUserbindingIndex  bool
		isGroupbindingIndex bool
		isSearch            bool
		notIn               bool

		property string
		keyword  string
		group    string

		userList *authv1.UserList
		err      error
	)

	authClient, err := mgr.AuthV1Client(request)
	if err != nil {
		return nil, err
	}

	k8sClient, err := mgr.Client(request)
	if err != nil {
		return nil, err
	}

	//authV1beta1Client, err := mgr.AuthV1beta1Client(request)
	//if err != nil {
	//	return nil, err
	//}
	//logger.Info("authV1beta1Client created")

	/*
		search eg: /v1/users?filterBy=project,myproj05,group,dev,username,小白&limit=1&continue=2

		users data source:
		1. userbindings index
		conditions: namespace, project, role_name, role_level, cluster


		2. userbindings + groupbindings index
		conditions: namespace, project, role_name, role_level, cluster, group

		3. groupbindings index
		conditions: group

		4. users index
		conditions: null

		5. fuzzy search
		conditions: email, username
	*/

	// Processing filterby
	for _, filter := range dataSelect.FilterQuery.FilterByList {
		filterValue := strings.TrimSpace(fmt.Sprintf("%s", filter.Value))
		switch filter.Property {
		case GroupProperty:
			isGroupbindingIndex = true
			group = filterValue
		case NamespaceProperty:
			isUserbindingIndex = true
			userbindingLabels = append(userbindingLabels, constant.LabelNamespace+"="+filterValue)
		case ProjectProperty:
			isUserbindingIndex = true
			userbindingLabels = append(userbindingLabels, constant.LabelProject+"="+filterValue)
		case RoleNameProperty:
			isUserbindingIndex = true
			userbindingLabels = append(userbindingLabels, constant.LabelRoleName+"="+filterValue)
		case RoleLevelProperty:
			isUserbindingIndex = true
			userbindingLabels = append(userbindingLabels, constant.LabelRoleLevel+"="+filterValue)
		case ClusterProperty:
			isUserbindingIndex = true
			userbindingLabels = append(userbindingLabels, constant.LabelCluster+"="+filterValue)
		case UsernameProperty, EmailProperty:
			isSearch = true
			property = string(filter.Property)
			keyword = filterValue
		case NotinProperty:
			notIn = true
		}
	}

	// when isSearch is true, will get all data index
	// exclusive users get all userbindings
	if notIn && isUserbindingIndex && isGroupbindingIndex {
		// userbindings + groupbindings not in
		userList, err = getUserListByNotInUserBindingsAndGroupBindings(authClient, k8sClient, userbindingLabels, group, isSearch, request)
		logger.Info("user list from not in userbindings + groupbindings intersection index")
	} else if notIn && isUserbindingIndex {
		// userbindings email not in all users
		userList, err = getUserListNotInUserbindings(authClient, userbindingLabels, isSearch, request)
		logger.Info("user list not in userbindings index users")
	} else if isUserbindingIndex && isGroupbindingIndex {
		// userbindings + groupbindings intersection index
		userList, err = getUserListByUserBindingsAndGroupBindings(authClient, k8sClient, userbindingLabels, group, isSearch, request)
		logger.Info("user list from userbindings + groupbindings intersection index")
	} else if isUserbindingIndex {
		// userbindings index
		userList, err = getUserListByUserBindings(authClient, k8sClient, userbindingLabels, isSearch, request)
		logger.Info("user list from userbindings index")
	} else if isGroupbindingIndex {
		// groupbindings index
		userList, err = getUserListByGroupBindings(authClient, group, isSearch, request)
		logger.Info("user list from groupbindings index")
	} else {
		// users index
		userList, err = getUserList(authClient, isSearch, request)
		logger.Info("user list from users index")
	}

	// username, email fuzzy search
	if isSearch {
		logger.Info("search users", zap.Any("property", property), zap.Any("keyword", keyword))
		return toUserList(userList.Items, dataSelect), nil
	}

	return userList, err
}

func getUserListNotInUserbindings(crdClient authclientapi.AuthV1Interface, labels []string, isSearch bool, request *restful.Request) (*authv1.UserList, error) {
	var (
		emails = make([]string, 0)
	)
	// list all userbindings
	userBindingList, err := crdClient.UserBindings().List(metav1.ListOptions{
		LabelSelector: strings.Join(labels, ","),
	})
	if err != nil {
		return nil, err
	}
	for _, binding := range userBindingList.Items {
		if email, ok := binding.Labels[constant.LabelUserEmail]; ok && email != "" {
			emails = append(emails, email)
		}
	}

	labelSelector := fmt.Sprintf(constant.LabelUserEmail+" notin (%s)", strings.Join(emails, ","))
	listOpts := metav1.ListOptions{}
	listOpts.LabelSelector = labelSelector
	if !isSearch {
		listOpts = parseRequestForListOpts(request, listOpts)
	}

	userList, err := crdClient.Users().List(listOpts)
	if err != nil {
		return nil, err
	}
	return userList, nil
}

func getUserListByNotInUserBindingsAndGroupBindings(crdClient authclientapi.AuthV1Interface, k8sClient kubernetes.Interface, labels []string, group string, isSearch bool, request *restful.Request) (*authv1.UserList, error) {
	var (
		emails = make([]string, 0)
	)
	// list all userbindings
	userBindingList, err := crdClient.UserBindings().List(metav1.ListOptions{
		LabelSelector: strings.Join(labels, ","),
	})
	if err != nil {
		return nil, err
	}
	for _, binding := range userBindingList.Items {
		if email, ok := binding.Labels[constant.LabelUserEmail]; ok && email != "" {
			emails = append(emails, email)
		}
	}

	// list emails by groups
	groupname := base58.Encode([]byte(group))
	groupbindingLabels := make([]string, 0)
	groupbindingLabels = append(groupbindingLabels, constant.LabelGroupDisplayName+"="+groupname)
	if len(emails) > 0 {
		groupbindingLabels = append(groupbindingLabels, fmt.Sprintf("%s notin (%s)", constant.LabelUserEmail, strings.Join(emails, ",")))
	}
	listOpts := metav1.ListOptions{}
	listOpts.LabelSelector = strings.Join(groupbindingLabels, ",")
	if !isSearch {
		listOpts = parseRequestForListOpts(request, listOpts)
	}
	groupBindingList, err := crdClient.GroupBindings().List(listOpts)
	if err != nil {
		return nil, err
	}
	emails = make([]string, 0)
	for _, binding := range groupBindingList.Items {
		if email, ok := binding.Labels[constant.LabelUserEmail]; ok && email != "" {
			emails = append(emails, email)
		}
	}

	// find users by emails
	userList, err := getUserListByEmails(crdClient, emails)
	if err != nil {
		return nil, err
	}
	return &authv1.UserList{
		Items:    userList.Items,
		ListMeta: groupBindingList.ListMeta,
	}, nil
}

func getUserListByUserBindingsAndGroupBindings(crdClient authclientapi.AuthV1Interface, k8sClient kubernetes.Interface, labels []string, group string, isSearch bool, request *restful.Request) (*authv1.UserList, error) {
	var (
		tmpUserBindingMap = make(map[string]authv1.UserBinding)
		tmpRoleNameMap    = make(map[string]struct{})
		tmpRoleMap        = make(map[string]rbacv1.ClusterRole)
		tmpUserMap        = make(map[string]authv1.User)
		emails            = make([]string, 0)
		place             = struct{}{}
	)
	// list all userbindings
	userBindingList, err := crdClient.UserBindings().List(metav1.ListOptions{
		LabelSelector: strings.Join(labels, ","),
	})
	if err != nil {
		return nil, err
	}

	// save tmp data
	for _, binding := range userBindingList.Items {
		if email, ok := binding.Labels[constant.LabelUserEmail]; ok && email != "" {
			emails = append(emails, email)
			//tmp userbindings
			tmpUserBindingMap[email] = binding
			if role, ok := binding.Labels[constant.LabelRoleName]; ok {
				// tmp users' roles
				tmpRoleNameMap[role] = place
			}
		}
	}

	// find groupbindings by emails
	groupname := base58.Encode([]byte(group))
	groupbindingLabels := make([]string, 0)
	groupbindingLabels = append(groupbindingLabels, constant.LabelGroupDisplayName+"="+groupname)
	if len(emails) > 0 {
		groupbindingLabels = append(groupbindingLabels, fmt.Sprintf("%s in (%s)", constant.LabelUserEmail, strings.Join(emails, ",")))
	}
	listOpts := metav1.ListOptions{}
	listOpts.LabelSelector = strings.Join(groupbindingLabels, ",")
	if !isSearch {
		listOpts = parseRequestForListOpts(request, listOpts)
	}
	groupBindingList, err := crdClient.GroupBindings().List(listOpts)
	if err != nil {
		return nil, err
	}
	emails = make([]string, 0)
	for _, binding := range groupBindingList.Items {
		if email, ok := binding.Labels[constant.LabelUserEmail]; ok && email != "" {
			emails = append(emails, email)
		}
	}

	// find roles by names
	roleNames := make([]string, 0)
	for k := range tmpRoleNameMap {
		roleNames = append(roleNames, k)
	}
	roleList, _ := getRoleListByNames(k8sClient, roleNames)
	if roleList != nil {
		for _, role := range roleList.Items {
			if name, ok := role.Labels[rc.LabelRoleRelative]; ok {
				tmpRoleMap[name] = role
			}
		}
	}

	// find users by emails
	userList, err := getUserListByEmails(crdClient, emails)
	if err != nil {
		return nil, err
	}
	for _, user := range userList.Items {
		tmpUserMap[user.Name] = user
	}

	// assemble the userlist as per groupbindings index
	// (user info must contain userbindings data for product)
	users := make([]authv1.User, 0)
	for _, groupbinding := range groupBindingList.Items {
		email, ok := groupbinding.Labels[constant.LabelUserEmail]
		if !ok || email == "" {
			continue
		}
		user, ok := tmpUserMap[email]
		if !ok {
			continue
		}
		userbinding, ok := tmpUserBindingMap[email]
		if !ok {
			continue
		}

		u := user.DeepCopy()
		if len(u.Annotations) == 0 {
			u.Annotations = make(map[string]string, 0)
		}
		// patch userbindings info for users
		u.Annotations["userbinding/name"] = userbinding.Name
		u.Annotations["userbinding/role.name"] = userbinding.Labels[constant.LabelRoleName]
		u.Annotations["userbinding/creationTimestamp"] = userbinding.CreationTimestamp.UTC().Format(time.RFC3339)
		u.Annotations["userbinding/role.display-name"] = ""
		if role, ok := tmpRoleMap[userbinding.Labels[constant.LabelRoleName]]; ok {
			if name, ok := role.Annotations[constant.AnnotationDisplayName]; ok {
				u.Annotations["userbinding/role.display-name"] = name
			}
		}
		users = append(users, *u)
	}
	return &authv1.UserList{
		Items:    users,
		ListMeta: userBindingList.ListMeta,
	}, nil
}

func getUserListByUserBindings(crdClient authclientapi.AuthV1Interface, k8sClient kubernetes.Interface, labels []string, isSearch bool, request *restful.Request) (*authv1.UserList, error) {
	var (
		tmpUserBindingMap = make(map[string]authv1.UserBinding)
		tmpRoleNameMap    = make(map[string]struct{})
		tmpRoleMap        = make(map[string]rbacv1.ClusterRole)
		tmpUserMap        = make(map[string]authv1.User)
		emails            = make([]string, 0)
		place             = struct{}{}
	)
	// list userbindings
	listOpts := metav1.ListOptions{}
	listOpts.LabelSelector = strings.Join(labels, ",")
	if !isSearch {
		listOpts = parseRequestForListOpts(request, listOpts)
	}
	userBindingList, err := crdClient.UserBindings().List(listOpts)
	if err != nil {
		return nil, err
	}

	// save tmp data
	for _, binding := range userBindingList.Items {
		if email, ok := binding.Labels[constant.LabelUserEmail]; ok && email != "" {
			emails = append(emails, email)
			// tmp userbinding
			tmpUserBindingMap[email] = binding
			if role, ok := binding.Labels[constant.LabelRoleName]; ok {
				// tmp users' roles
				tmpRoleNameMap[role] = place
			}
		}
	}

	// find users by emails
	userList, err := getUserListByEmails(crdClient, emails)
	if err != nil {
		return nil, err
	}
	for _, user := range userList.Items {
		tmpUserMap[user.Name] = user
	}

	// find roles by names
	roleNames := make([]string, 0)
	for k := range tmpRoleNameMap {
		roleNames = append(roleNames, k)
	}
	roleList, _ := getRoleListByNames(k8sClient, roleNames)
	if roleList != nil {
		for _, role := range roleList.Items {
			logger.Info(fmt.Sprintf("getRoleListByNames, RoleName:%v", role.Name))
			if name, ok := role.Labels[rc.LabelRoleRelative]; ok {
				tmpRoleMap[name] = role
			}
		}
	}

	// assemble the user list as per userbindings index
	// (user data must contain userbindings data for product)
	users := make([]authv1.User, 0)
	logger.Debug("binding", zap.Any("Items.count", len(userBindingList.Items)))
	for _, binding := range userBindingList.Items {
		email, ok := binding.Labels[constant.LabelUserEmail]
		if !ok || email == "" {
			continue
		}
		user, ok := tmpUserMap[email]
		if !ok {
			continue
		}

		u := user.DeepCopy()
		if len(u.Annotations) == 0 {
			u.Annotations = make(map[string]string, 0)
		}
		// patch userbindings info for users
		u.Annotations["userbinding/name"] = binding.Name
		u.Annotations["userbinding/role.name"] = binding.Labels[constant.LabelRoleName]
		u.Annotations["userbinding/creationTimestamp"] = binding.CreationTimestamp.UTC().Format(time.RFC3339)
		u.Annotations["userbinding/role.display-name"] = ""
		if role, ok := tmpRoleMap[binding.Labels[constant.LabelRoleName]]; ok {
			if name, ok := role.Annotations[constant.AnnotationDisplayName]; ok {
				logger.Info(fmt.Sprintf("annotations role.name:%v", name))
				u.Annotations["userbinding/role.display-name"] = name
			}
		}
		users = append(users, *u)
	}
	return &authv1.UserList{
		Items:    users,
		ListMeta: userBindingList.ListMeta,
	}, nil
}

func getUserListByGroupBindings(client authclientapi.AuthV1Interface, group string, isSearch bool, request *restful.Request) (*authv1.UserList, error) {
	// list groupbindings
	listOpts := metav1.ListOptions{}
	listOpts.LabelSelector = constant.LabelGroupDisplayName + "=" + base58.Encode([]byte(group))
	if !isSearch {
		listOpts = parseRequestForListOpts(request, listOpts)
	}
	groupBindingList, err := client.GroupBindings().List(listOpts)
	if err != nil {
		return nil, err
	}
	emails := make([]string, 0)
	for _, binding := range groupBindingList.Items {
		if email, ok := binding.Labels[constant.LabelUserEmail]; ok {
			emails = append(emails, email)
		}
	}

	// find users by emails
	userList, err := getUserListByEmails(client, emails)
	if err != nil {
		return nil, err
	}
	userList.ListMeta = groupBindingList.ListMeta
	return userList, nil
}

func getUserList(client authclientapi.AuthV1Interface, isSearch bool, request *restful.Request) (*authv1.UserList, error) {
	// list userList
	listOpts := metav1.ListOptions{}
	if !isSearch {
		listOpts = parseRequestForListOpts(request, listOpts)
	}
	userList, err := client.Users().List(listOpts)
	if err != nil {
		return nil, err
	}
	return userList, nil
}

func parseRequestForListOpts(request *restful.Request, listOpts metav1.ListOptions) metav1.ListOptions {
	if limitStr := request.QueryParameter("limit"); limitStr != "" {
		listOpts.Limit, _ = strconv.ParseInt(limitStr, 10, 64)
	}
	if continueStr := request.QueryParameter("continue"); continueStr != "" {
		listOpts.Continue = continueStr
	}
	return listOpts
}

func getUserListByEmails(client authclientapi.AuthV1Interface, emails []string) (userList *authv1.UserList, err error) {
	logger.Debug("getUserListByEmails", zap.Any("emails", emails))
	if len(emails) == 0 {
		return NewUserList(), nil
	}

	//labelSelector := fmt.Sprintf("auth.alauda.io/user.email in (%s)", strings.Join(emails, ","))
	//return client.Users().List(metav1.ListOptions{
	//	LabelSelector: labelSelector,
	//})

	users := make([]authv1.User, 0)
	for _, email := range emails {
		ch := make(chan *authv1.User)
		go func(client authclientapi.AuthV1Interface, name string, chnl chan *authv1.User) {
			defer close(chnl)
			user, err := client.Users().Get(name, metav1.GetOptions{})
			if err != nil {
				logger.Error("getUserListByEmails", zap.Any("name", name), zap.Any("err", err))
				return
			}
			chnl <- user
		}(client, email, ch)
		for user := range ch {
			users = append(users, *user)
		}
	}

	return &authv1.UserList{
		Items: users,
	}, nil
}

func NewUserList() *authv1.UserList {
	return &authv1.UserList{
		Items: []authv1.User{},
	}
}

func toUserList(users []authv1.User, dsQuery *dataselect.Query) *authv1.UserList {
	userList := &authv1.UserList{
		Items: make([]authv1.User, 0),
	}

	filterByList := make([]dataselect.FilterBy, 0)
	for _, filter := range dsQuery.FilterQuery.FilterByList {
		switch filter.Property {
		case EmailProperty, UsernameProperty:
			filterByList = append(filterByList, filter)
		}
	}
	dsQuery.FilterQuery.FilterByList = filterByList
	logger.Debug("toUserList", zap.Any("dsQuery", dsQuery))

	userCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(users), dsQuery)
	users = fromCells(userCells)

	// calculate has next page
	nextPage := ""
	if hasNextPage(dsQuery.PaginationQuery.Page, dsQuery.PaginationQuery.ItemsPerPage, filteredTotal) {
		nextPage = strconv.Itoa(dsQuery.PaginationQuery.Page + 2)
	}
	userList.ListMeta = metav1.ListMeta{
		Continue: nextPage,
	}
	logger.Debug("filteredTotal", zap.Any("filteredTotal", filteredTotal), zap.Any("nextPage", nextPage))

	for _, user := range users {
		userList.Items = append(userList.Items, user)
	}

	return userList
}

func hasNextPage(currpage, limit, total int) bool {
	start := currpage
	if start < 0 {
		start = 0
	}
	start = start*limit + 1

	end := start - 1 + limit
	if end > total {
		end = total
	}
	logger.Debug("hasNextPage", zap.Any("end", end), zap.Any("start", start), zap.Any("limit", limit), zap.Any("total", total))
	return end < total
}

func getRoleListByNames(client kubernetes.Interface, roles []string) (roleList *rbacv1.ClusterRoleList, err error) {
	if len(roles) == 0 {
		return &rbacv1.ClusterRoleList{}, nil
	}

	labelSelector := fmt.Sprintf("%s,%s in (%s)", rc.LabelRoleRelative, rc.LabelRoleRelative, strings.Join(roles, ","))
	cli := client.RbacV1().ClusterRoles()
	return cli.List(metav1.ListOptions{
		LabelSelector: labelSelector,
	})
}

func getRoleTemplateListByNames(crdClient authclientv1beta1.AuthV1Beta1Interface, roles []string) (roleList *authv1beta1.RoleTemplateList, err error) {
	if len(roles) == 0 {
		return &authv1beta1.RoleTemplateList{}, nil
	}

	labelSelector := fmt.Sprintf("%s,%s in (%s)", constant.LabelRoleTemplateName, constant.LabelRoleTemplateName, strings.Join(roles, ","))
	roleTemplateList, err := crdClient.RoleTemplates().List(metav1.ListOptions{
		LabelSelector: labelSelector,
	})
	if err != nil {
		logger.Info(fmt.Sprintf("err:%v", err))
	}
	return roleTemplateList, err
}

func (apiHandler *APIHandler) handleGetUser(request *restful.Request, res *restful.Response) {

	var authClient authclientapi.AuthV1Interface

	self, err := CheckRequestIsSelf(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	if self {
		authClient, err = apiHandler.cManager.InsecureAuthV1Client()
	} else {
		authClient, err = apiHandler.cManager.AuthV1Client(request)
		if err != nil {
			apiHandler.WriteError(res, err)
			return
		}
	}

	name := request.PathParameter("name")
	user, err := authClient.Users().Get(name, metav1.GetOptions{})
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	res.WriteHeaderAndEntity(http.StatusOK, user)
}

func (apiHandler *APIHandler) handleDeleteInvalidUsers(request *restful.Request, res *restful.Response) {
	users := []string{}
	clearRangeStr := request.QueryParameter("range")
	labelSelector := constant.LabelUserValid + "=false"
	if clearRangeStr != "all" {
		if err := request.ReadEntity(&users); err != nil {
			apiHandler.WriteError(res, err)
			return
		}
		validNames := make([]string, 0)
		for _, v := range users {
			if len(v) > 0 {
				validNames = append(validNames, v)
			}
		}

		if len(validNames) == 0 {
			//apiHandler.WriteError(res, errors.New("params invalid"))
			res.WriteHeader(http.StatusOK)
			return
		}

		labelSelector = fmt.Sprintf("%s=false,%s in (%s)", constant.LabelUserValid, constant.LabelUserEmail, strings.Join(validNames, ","))
	}

	crdClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	err = crdClient.Users().DeleteCollection(&metav1.DeleteOptions{}, metav1.ListOptions{
		LabelSelector: labelSelector,
	})
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	res.WriteHeader(http.StatusOK)
}
