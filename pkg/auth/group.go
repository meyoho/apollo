package auth

import (
	"net/http"

	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (apiHandler *APIHandler) handleGetGroups(request *restful.Request, res *restful.Response) {

	crdClient, err := apiHandler.cManager.AuthV1Client(request)
	if err != nil {
		apiHandler.WriteError(res, err)
	}

	listOpts := metav1.ListOptions{}
	list, err := crdClient.Groups().List(listOpts)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}
	if list != nil {
		count := len(list.Items)
		for i, item := range list.Items {
			switch item.Annotations[constant.AnnotationDisplayName] {
			case ungrouped, ungroupedCN:
				if i == count-1 {
					continue
				}
				list.Items = append(list.Items[:i], list.Items[i+1:]...)
				if item.Annotations[constant.AnnotationDisplayName] == ungrouped {
					list.Items = append(list.Items, item)
				}
			}
		}
	}

	res.WriteHeaderAndEntity(http.StatusOK, list)
}
