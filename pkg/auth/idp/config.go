package idp

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"

	"golang.org/x/crypto/bcrypt"

	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/authproxy"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/bitbucketcloud"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/github"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/gitlab"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/keystone"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/ldap"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/linkedin"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/microsoft"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/mock"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/oidc"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/connector/saml"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/log"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/storage"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/storage/kubernetes"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp/storage/kubernetes/k8sapi"
)

// Copyed from dex/server
// ConnectorConfig is a configuration that can open a connector.
type ConnectorConfig interface {
	Open(id string, logger log.Logger) (connector.Connector, error)
}

// ConnectorsConfig variable provides an easy way to return a config struct
// depending on the connector type.
var ConnectorsConfig = map[string]func() ConnectorConfig{
	"keystone":        func() ConnectorConfig { return new(keystone.Config) },
	"mockCallback":    func() ConnectorConfig { return new(mock.CallbackConfig) },
	"mockPassword":    func() ConnectorConfig { return new(mock.PasswordConfig) },
	"ldap":            func() ConnectorConfig { return new(ldap.Config) },
	"github":          func() ConnectorConfig { return new(github.Config) },
	"gitlab":          func() ConnectorConfig { return new(gitlab.Config) },
	"oidc":            func() ConnectorConfig { return new(oidc.Config) },
	"saml":            func() ConnectorConfig { return new(saml.Config) },
	"authproxy":       func() ConnectorConfig { return new(authproxy.Config) },
	"linkedin":        func() ConnectorConfig { return new(linkedin.Config) },
	"microsoft":       func() ConnectorConfig { return new(microsoft.Config) },
	"bitbucket-cloud": func() ConnectorConfig { return new(bitbucketcloud.Config) },
	// Keep around for backwards compatibility.
	"samlExperimental": func() ConnectorConfig { return new(saml.Config) },
}

// openConnector will parse the connector config and open the connector.
func openConnector(logger log.Logger, conn storage.Connector) (connector.Connector, error) {
	var c connector.Connector

	f, ok := ConnectorsConfig[conn.Type]
	if !ok {
		return c, fmt.Errorf("unknown connector type %q", conn.Type)
	}

	connConfig := f()
	if len(conn.Config) != 0 {
		data := []byte(string(conn.Config))
		if err := json.Unmarshal(data, connConfig); err != nil {
			return c, fmt.Errorf("parse connector config: %v", err)
		}
	}

	c, err := connConfig.Open(conn.ID, logger)
	if err != nil {
		return c, fmt.Errorf("failed to create connector %s: %v", conn.ID, err)
	}

	return c, nil
}

// WebConfig holds the server's frontend templates and asset configuration.
//
// These are currently very custom to CoreOS and it's not recommended that
// outside users attempt to customize these.
type WebConfig struct {
	// A filepath to web static.
	//
	// It is expected to contain the following directories:
	//
	//   * static - Static static served at "( issuer URL )/static".
	//   * templates - HTML templates controlled by dex.
	//   * themes/(theme) - Static static served at "( issuer URL )/theme".
	//
	Dir string

	// Defaults to "( issuer URL )/theme/logo.png"
	LogoURL string

	// Defaults to "dex"
	Issuer string

	// Defaults to "coreos"
	Theme string
}

// endof copy from dex/server

// Config is the config format for the main application.
type Config struct {
	Issuer    string    `json:"issuer"`
	Storage   Storage   `json:"storage"`
	Web       Web       `json:"web"`
	Telemetry Telemetry `json:"telemetry"`
	OAuth2    OAuth2    `json:"oauth2"`
	GRPC      GRPC      `json:"grpc"`
	Expiry    Expiry    `json:"expiry"`
	Logger    Logger    `json:"logger"`

	Frontend WebConfig `json:"frontend"`

	// StaticConnectors are user defined connectors specified in the ConfigMap
	// Write operations, like updating a connector, will fail.
	StaticConnectors []Connector `json:"connectors"`

	// StaticClients cause the server to use this list of clients rather than
	// querying the storage. Write operations, like creating a client, will fail.
	StaticClients []storage.Client `json:"staticClients"`

	// If enabled, the server will maintain a list of passwords which can be used
	// to identify a user.
	EnablePasswordDB bool `json:"enablePasswordDB"`

	// StaticPasswords cause the server use this list of passwords rather than
	// querying the storage. Cannot be specified without enabling a passwords
	// database.
	StaticPasswords []password `json:"staticPasswords"`
}

type password storage.Password

func (p *password) UnmarshalJSON(b []byte) error {
	var data struct {
		Email    string `json:"email"`
		Username string `json:"username"`
		UserID   string `json:"userID"`
		Hash     string `json:"hash"`
	}
	if err := json.Unmarshal(b, &data); err != nil {
		return err
	}
	*p = password(storage.Password{
		Email:    data.Email,
		Username: data.Username,
		UserID:   data.UserID,
	})
	if len(data.Hash) == 0 {
		return fmt.Errorf("no password hash provided")
	}

	// If this value is a valid bcrypt, use it.
	_, bcryptErr := bcrypt.Cost([]byte(data.Hash))
	if bcryptErr == nil {
		p.Hash = []byte(data.Hash)
		return nil
	}

	// For backwards compatibility try to base64 decode this value.
	hashBytes, err := base64.StdEncoding.DecodeString(data.Hash)
	if err != nil {
		return fmt.Errorf("malformed bcrypt hash: %v", bcryptErr)
	}
	if _, err := bcrypt.Cost(hashBytes); err != nil {
		return fmt.Errorf("malformed bcrypt hash: %v", err)
	}
	p.Hash = hashBytes
	return nil
}

// OAuth2 describes enabled OAuth2 extensions.
type OAuth2 struct {
	ResponseTypes []string `json:"responseTypes"`
	// If specified, do not prompt the user to approve client authorization. The
	// act of logging in implies authorization.
	SkipApprovalScreen bool `json:"skipApprovalScreen"`
}

// Web is the config format for the HTTP server.
type Web struct {
	HTTP           string   `json:"http"`
	HTTPS          string   `json:"https"`
	TLSCert        string   `json:"tlsCert"`
	TLSKey         string   `json:"tlsKey"`
	AllowedOrigins []string `json:"allowedOrigins"`
}

// Telemetry is the config format for telemetry including the HTTP server config.
type Telemetry struct {
	HTTP string `json:"http"`
}

// GRPC is the config for the gRPC API.
type GRPC struct {
	// The port to listen on.
	Addr        string `json:"addr"`
	TLSCert     string `json:"tlsCert"`
	TLSKey      string `json:"tlsKey"`
	TLSClientCA string `json:"tlsClientCA"`
}

// Storage holds app's storage configuration.
type Storage struct {
	Type   string        `json:"type"`
	Config StorageConfig `json:"config"`
}

// StorageConfig is a configuration that can create a storage.
type StorageConfig interface {
	Open(logger log.Logger) (storage.Storage, error)
}

var storages = map[string]func() StorageConfig{
	"kubernetes": func() StorageConfig { return new(kubernetes.Config) },
}

// UnmarshalJSON allows Storage to implement the unmarshaler interface to
// dynamically determine the type of the storage config.
func (s *Storage) UnmarshalJSON(b []byte) error {
	var store struct {
		Type   string          `json:"type"`
		Config json.RawMessage `json:"config"`
	}
	if err := json.Unmarshal(b, &store); err != nil {
		return fmt.Errorf("parse storage: %v", err)
	}
	f, ok := storages[store.Type]
	if !ok {
		return fmt.Errorf("unknown storage type %q", store.Type)
	}

	storageConfig := f()
	if len(store.Config) != 0 {
		data := []byte(os.ExpandEnv(string(store.Config)))
		if err := json.Unmarshal(data, storageConfig); err != nil {
			return fmt.Errorf("parse storage config: %v", err)
		}
	}
	*s = Storage{
		Type:   store.Type,
		Config: storageConfig,
	}
	return nil
}

type IDPValidation struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type ConnectorWrapper struct {
	k8sapi.ObjectMeta `json:"metadata,omitempty"`
	k8sapi.TypeMeta   `json:",inline"`

	Type string `json:"type"`
	Name string `json:"name"`
	ID   string `json:"id"`

	Config ConnectorConfig `json:"config"`

	IDPValidation IDPValidation `json:"idpValidation,omitempty"`
}

// Connector is a magical type that can unmarshal YAML dynamically. The
// Type field determines the connector type, which is then customized for Config.
type Connector struct {
	k8sapi.ObjectMeta `json:"metadata,omitempty"`
	k8sapi.TypeMeta   `json:",inline"`

	Type string `json:"type"`
	Name string `json:"name"`
	ID   string `json:"id"`

	Config ConnectorConfig `json:"config"`
}

// UnmarshalJSON allows Connector to implement the unmarshaler interface to
// dynamically determine the type of the connector config.
func (c *Connector) UnmarshalJSON(b []byte) error {
	var conn struct {
		k8sapi.ObjectMeta `json:"metadata,omitempty"`
		k8sapi.TypeMeta   `json:",inline"`

		Type string `json:"type"`
		Name string `json:"name"`
		ID   string `json:"id"`

		Config json.RawMessage `json:"config"`
	}
	if err := json.Unmarshal(b, &conn); err != nil {
		return fmt.Errorf("parse connector: %v", err)
	}
	f, ok := ConnectorsConfig[conn.Type]
	if !ok {
		return fmt.Errorf("unknown connector type %q", conn.Type)
	}

	connConfig := f()
	if len(conn.Config) != 0 {
		data := []byte(os.ExpandEnv(string(conn.Config)))
		if err := json.Unmarshal(data, connConfig); err != nil {
			return fmt.Errorf("parse connector config: %v", err)
		}
	}
	*c = Connector{
		ObjectMeta: conn.ObjectMeta,
		TypeMeta:   conn.TypeMeta,
		Type:       conn.Type,
		Name:       conn.Name,
		ID:         conn.ID,
		Config:     connConfig,
	}
	return nil
}

func (c *ConnectorWrapper) ToConnector() Connector {
	return Connector{
		ObjectMeta: c.ObjectMeta,
		TypeMeta:   c.TypeMeta,
		Type:       c.Type,
		Name:       c.Name,
		ID:         c.ID,
		Config:     c.Config,
	}
}

// UnmarshalJSON allows Connector to implement the unmarshaler interface to
// dynamically determine the type of the connector config.
func (c *ConnectorWrapper) UnmarshalJSON(b []byte) error {
	var conn struct {
		k8sapi.ObjectMeta `json:"metadata,omitempty"`
		k8sapi.TypeMeta   `json:",inline"`

		Type string `json:"type"`
		Name string `json:"name"`
		ID   string `json:"id"`

		Config json.RawMessage `json:"config"`

		IDPValidation IDPValidation `json:"idpValidation"`
	}
	if err := json.Unmarshal(b, &conn); err != nil {
		return fmt.Errorf("parse connector: %v", err)
	}
	f, ok := ConnectorsConfig[conn.Type]
	if !ok {
		return fmt.Errorf("unknown connector type %q", conn.Type)
	}

	connConfig := f()
	if len(conn.Config) != 0 {
		data := []byte(os.ExpandEnv(string(conn.Config)))
		if err := json.Unmarshal(data, connConfig); err != nil {
			return fmt.Errorf("parse connector config: %v", err)
		}
	}
	*c = ConnectorWrapper{
		ObjectMeta:    conn.ObjectMeta,
		TypeMeta:      conn.TypeMeta,
		Type:          conn.Type,
		Name:          conn.Name,
		ID:            conn.ID,
		Config:        connConfig,
		IDPValidation: conn.IDPValidation,
	}
	return nil
}

// ToStorageConnector converts an object to storage connector type.
func ToStorageConnector(c Connector) (storage.Connector, error) {
	data, err := json.Marshal(c.Config)
	if err != nil {
		return storage.Connector{}, fmt.Errorf("failed to marshal connector config: %v", err)
	}

	return storage.Connector{
		ObjectMeta: c.ObjectMeta,
		TypeMeta:   c.TypeMeta,
		ID:         c.ID,
		Type:       c.Type,
		Name:       c.Name,
		Config:     data,
	}, nil
}

// FromStorageConnector converts an storage connector to connector.
func FromStorageConnector(c storage.Connector) (*Connector, error) {
	f, ok := ConnectorsConfig[c.Type]
	if !ok {
		return nil, fmt.Errorf("unknown connector type %q", c.Type)
	}

	connConfig := f()
	if len(c.Config) != 0 {
		data := []byte(os.ExpandEnv(string(c.Config)))
		if err := json.Unmarshal(data, connConfig); err != nil {
			return nil, fmt.Errorf("parse connector config: %v", err)
		}
	}
	return &Connector{
		ObjectMeta: c.ObjectMeta,
		TypeMeta:   c.TypeMeta,
		Type:       c.Type,
		Name:       c.Name,
		ID:         c.ID,
		Config:     connConfig,
	}, nil
}

// Expiry holds configuration for the validity period of components.
type Expiry struct {
	// SigningKeys defines the duration of time after which the SigningKeys will be rotated.
	SigningKeys string `json:"signingKeys"`

	// IdTokens defines the duration of time for which the IdTokens will be valid.
	IDTokens string `json:"idTokens"`

	// AuthRequests defines the duration of time for which the AuthRequests will be valid.
	AuthRequests string `json:"authRequests"`
}

// Logger holds configuration required to customize logging for dex.
type Logger struct {
	// Level sets logging level severity.
	Level string `json:"level"`

	// Format specifies the format to be used for logging.
	Format string `json:"format"`
}
