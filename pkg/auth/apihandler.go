package auth

import (
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"

	"k8s.io/apimachinery/pkg/api/errors"

	"bitbucket.org/mathildetech/apollo/api"
	"bitbucket.org/mathildetech/apollo/pkg/auth/idp"

	"bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	clientapi "bitbucket.org/mathildetech/apollo/client/api"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"

	authv1beta1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1beta1"
	"github.com/emicklei/go-restful"
	"go.uber.org/zap"
	authorizationv1 "k8s.io/api/authorization/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Auth conole data
type APIHandler struct {
	cManager clientapi.ClientManager
}

var logger *zap.Logger

// New creates new instance
func CreateHTTPAPIHandler() *APIHandler {
	return &APIHandler{}
}

func (apiHandler *APIHandler) WriteError(res *restful.Response, err error) {
	statusErr, ok := err.(*errors.StatusError)
	if !ok {
		statusErr = errors.NewInternalError(err)
	}
	statusErr.ErrStatus.Kind = "Status"
	statusErr.ErrStatus.APIVersion = "v1"
	res.WriteHeaderAndJson(
		int(statusErr.Status().Code),
		statusErr.Status(),
		restful.MIME_JSON)
}

// ApplyToServer apply to server
func (apiHandler *APIHandler) ApplyToServer(srv server.Server, cManager clientapi.ClientManager) error {
	apiHandler.cManager = cManager

	gen := decorator.NewWSGenerator()
	ws := gen.New(srv)
	logger = srv.L().Named("auth")
	ws.Doc("Basic APIs for Apollo")
	ws.Path("/v1")

	ws.Route(
		ws.PUT("users/sync").
			To(apiHandler.handleSyncUsers).
			Doc("Sync users from idp").
			Writes(metav1.Status{}).
			Returns(200, "OK", metav1.Status{}),
	)

	ws.Route(
		ws.GET("users").
			To(apiHandler.handleGetUserList).
			Doc("Fetch users").
			Writes(authv1.UserList{}).
			Returns(http.StatusOK, "OK", authv1.UserList{}),
	)

	ws.Route(
		ws.POST("users").
			To(apiHandler.HandleCreateUser).
			Doc("create user").
			Writes(authv1.User{}).
			Returns(http.StatusCreated, "OK", authv1.User{}),
	)

	ws.Route(
		ws.PATCH("users/{name}").
			To(apiHandler.HandlePatchUser).
			Doc("modify user's password and display-name").
			Writes(authv1.User{}).
			Returns(http.StatusCreated, "OK", authv1.User{}),
	)

	ws.Route(
		ws.DELETE("users/{name}").
			To(apiHandler.HandleDeleteUser).
			Doc("delete specific local users").
			Returns(http.StatusCreated, "OK", nil),
	)

	ws.Route(
		ws.GET("usersroles").
			To(apiHandler.HandleGetUsersRoles).
			Doc("get users roles").
			Returns(http.StatusOK, "OK", nil),
	)

	ws.Route(
		ws.GET("users/{name}").
			To(apiHandler.handleGetUser).
			Doc("Fetch user info").
			Writes(authv1.User{}).
			Returns(http.StatusOK, "OK", authv1.User{}),
	)

	ws.Route(
		ws.GET("roles/{name}/rules").
			To(apiHandler.handleGetRoleRules).
			Doc("Fetch role rules").
			Writes(api.RoleRuleList{}).
			Returns(http.StatusOK, "OK", api.RoleRuleList{}),
	)

	ws.Route(
		ws.GET("resources/{resource_type}/groups/{api_group}").
			To(apiHandler.handleGetResourceFilter).
			Doc("Fetch resource filter").
			Writes(api.ResourcesList{}).
			Returns(http.StatusOK, "OK", api.ResourcesList{}),
	)

	ws.Route(
		ws.GET("projects/available-resources").
			To(apiHandler.HandleProjectResources).
			Doc("Fetch available resources for creating new project").
			Writes(authv1.ClusterQuotaList{}).
			Returns(http.StatusOK, "OK", authv1.ClusterQuotaList{}),
	)

	ws.Route(
		ws.GET("projects/{name}/clusters/{cluster-name}/namespaces").
			To(apiHandler.HandleProjectNamespaces).
			Doc("Fetch namespaces, filtered by rbac").
			Writes(corev1.NamespaceList{}).
			Returns(http.StatusOK, "OK", corev1.NamespaceList{}),
	)

	ws.Route(
		ws.GET("projects/{name}/clusters/{cluster-name}/federatednamespaces").
			To(apiHandler.HandleProjectFederatedNamespaces).
			Doc("Fetch federated namespaces, filtered by rbac").
			Writes(unstructured.UnstructuredList{}).
			Returns(http.StatusOK, "OK", unstructured.UnstructuredList{}),
	)

	ws.Route(
		ws.GET("projects").
			To(apiHandler.HandleProjectList).
			Doc("Fetch project list with rbac").
			Writes(authv1.ProjectList{}).
			Returns(http.StatusOK, "OK", authv1.ProjectList{}),
	)

	ws.Route(
		ws.POST("userbindings").
			To(apiHandler.HandleCreateUserBinding).
			Doc("Create userbindings").
			Writes(authv1.UserBinding{}).
			Returns(http.StatusCreated, "OK", authv1.UserBinding{}),
	)

	ws.Route(
		ws.DELETE("userbindings/{name}").
			To(apiHandler.HandleDeleteUserBinding).
			Doc("Delete userbindings").
			Writes(authv1.UserBinding{}).
			Returns(http.StatusOK, "OK", authv1.UserBinding{}),
	)

	ws.Route(
		ws.POST("selfsubjectaccessreviews").
			To(apiHandler.HandleCreateSelfSubjectAccessReview).
			Doc("Verify resource").
			Writes(authorizationv1.SelfSubjectAccessReview{}).
			Returns(http.StatusCreated, "OK", authorizationv1.SelfSubjectAccessReview{}),
	)

	// create connectors
	ws.Route(
		ws.POST("connectors").
			To(apiHandler.HandleConnectors).
			Doc("Create dex connectors").
			Writes(idp.Connector{}).
			Returns(http.StatusCreated, "OK", idp.Connector{}),
	)
	// patch connectors
	ws.Route(
		ws.PUT("connectors/{connector-id}").
			To(apiHandler.HandleConnectors).
			Doc("Patch dex connectors").
			Writes(idp.Connector{}).
			Returns(http.StatusOK, "OK", idp.Connector{}),
	)
	// delete connectors
	ws.Route(
		ws.DELETE("connectors/{connector-id}").
			To(apiHandler.HandleDeleteConnectors).
			Doc("Delete dex connectors").
			Writes(idp.Connector{}).
			Returns(http.StatusOK, "OK", idp.Connector{}),
	)

	ws.Route(
		ws.DELETE("users").
			To(apiHandler.handleDeleteInvalidUsers).
			Doc("Delete invalid users").
			Writes(authv1.UserList{}).
			Returns(http.StatusOK, "OK", authv1.UserList{}),
	)

	ws.Route(
		ws.GET("groups").
			To(apiHandler.handleGetGroups).
			Doc("Get groups").
			Writes(authv1.GroupList{}).
			Returns(http.StatusOK, "OK", authv1.GroupList{}),
	)

	ws.Route(
		ws.DELETE("roletemplates/{name}").
			To(apiHandler.HandleDeleteRoleTemplate).
			Doc("Delete roletemplates").
			Writes(authv1beta1.RoleTemplate{}).
			Returns(http.StatusOK, "OK", authv1beta1.RoleTemplate{}),
	)

	ws.Route(
		ws.GET("products").
			To(apiHandler.handleGetProducts).
			Doc("Fetch products").
			Writes(ProductList{}).
			Returns(http.StatusOK, "OK", ProductList{}),
	)

	ws.Route(
		ws.GET("products/{name}").
			To(apiHandler.handleGetProduct).
			Doc("Fetch products by crd name").
			Writes(ProductItem{}).
			Returns(http.StatusOK, "OK", ProductItem{}),
	)

	srv.Container().Add(ws)

	return nil
}

type FilterQuery struct {
	FilterByList []FilterBy
}

type FilterBy struct {
	Property string
	Value    string
}

var NoFilter = &FilterQuery{
	FilterByList: []FilterBy{},
}

// NewFilterQuery takes raw filter options list and returns FilterQuery object. For example:
// ["parameter1", "value1", "parameter2", "value2"] - means that the data should be filtered by
// parameter1 equals value1 and parameter2 equals value2
func NewFilterQuery(filterByListRaw []string) *FilterQuery {
	if filterByListRaw == nil || len(filterByListRaw)%2 == 1 {
		return NoFilter
	}
	filterByList := []FilterBy{}
	for i := 0; i+1 < len(filterByListRaw); i += 2 {
		propertyName := filterByListRaw[i]
		propertyValue := filterByListRaw[i+1]
		filterBy := FilterBy{
			Property: string(propertyName),
			Value:    string(propertyValue),
		}
		// Add to the filter options.
		filterByList = append(filterByList, filterBy)
	}
	return &FilterQuery{
		FilterByList: filterByList,
	}
}

func parseFilterPathParameter(request *restful.Request) *FilterQuery {
	return NewFilterQuery(strings.Split(request.QueryParameter("filterBy"), ","))
}

// parseListOpts will parse the supported query params to construct a ListOptions
func parseListOpts(req *restful.Request, isFuzzySearch bool) (*metav1.ListOptions, error) {
	listOpts := &metav1.ListOptions{}
	var err error

	labelSelector := req.QueryParameter("labelSelector")
	if labelSelector != "" {
		listOpts.LabelSelector = labelSelector
	}

	fieldSelector := req.QueryParameter("fieldSelector")
	if fieldSelector != "" {
		listOpts.FieldSelector = fieldSelector
	}

	watchStr := req.QueryParameter("watch")
	if watchStr != "" {
		watch, err := strconv.ParseBool(watchStr)
		if err != nil {
			return nil, err
		}
		listOpts.Watch = watch
	}

	resourceVersion := req.QueryParameter("resourceVersion")
	if resourceVersion != "" {
		listOpts.ResourceVersion = resourceVersion
	}

	timeoutSecondsStr := req.QueryParameter("timeoutSeconds")
	if timeoutSecondsStr != "" {
		timeoutSeconds, err := strconv.ParseInt(timeoutSecondsStr, 10, 0)
		if err != nil {
			return nil, err
		}
		listOpts.TimeoutSeconds = &timeoutSeconds
	}

	// if isFuzzySearch then limit and contiue is handled by dataselect
	limitStr := req.QueryParameter("limit")
	if !isFuzzySearch && limitStr != "" {
		limit, err := strconv.ParseInt(limitStr, 10, 0)
		if err != nil {
			return nil, err
		}
		listOpts.Limit = limit
	}

	continueStr := req.QueryParameter("continue")
	if !isFuzzySearch && continueStr != "" {
		listOpts.Continue = continueStr
	}

	return listOpts, err
}

// Parses query parameters of the request and returns a DataSelectQuery object
func parseDataSelectPathParameter(request *restful.Request) *dataselect.Query {
	paginationQuery := parsePaginationPathParameters(request)
	sortQuery := parseSortPathParameters(request)
	filterQuery := parseFilterPathParameters(request)
	return dataselect.NewDataSelectQuery(paginationQuery, sortQuery, filterQuery)
}

func parsePaginationPathParameters(request *restful.Request) *dataselect.PaginationQuery {
	limitStr := request.QueryParameter("limit")
	if limitStr == "" {
		limitStr = strconv.Itoa(20)
	}
	itemsPerPage, _ := strconv.ParseInt(limitStr, 10, 0)

	continueStr := request.QueryParameter("continue")
	if continueStr == "" {
		continueStr = strconv.Itoa(1)
	}
	page, _ := strconv.ParseInt(continueStr, 10, 0)

	// Frontend pages start from 1 and backend starts from 0
	return dataselect.NewPaginationQuery(int(itemsPerPage), int(page-1))
}

// Parses query parameters of the request and returns a SortQuery object
func parseSortPathParameters(request *restful.Request) *dataselect.SortQuery {
	return dataselect.NewSortQuery(strings.Split(request.QueryParameter("sortBy"), ","))
}

func parseFilterPathParameters(request *restful.Request) *dataselect.FilterQuery {
	//log.Printf("------- QueryParameter filterBy before: %+v", request.QueryParameter("filterBy"))
	filters := strings.Split(request.QueryParameter("filterBy"), ",")
	for k, v := range filters {
		filters[k], _ = url.QueryUnescape(v)
		filters[k] = strings.Replace(filters[k], "&#44;", ",", -1)
	}
	log.Printf("------------- filterBy: %+v", filters)
	return dataselect.NewFilterQuery(filters)
}
