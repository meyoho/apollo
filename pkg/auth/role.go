package auth

import (
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/mathildetech/apollo/api"
	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (apiHandler *APIHandler) handleGetRoleRules(request *restful.Request, res *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	roleName := request.PathParameter("name")

	label := fmt.Sprintf("%s,%s=%s", constant.LabelRoleRelative, constant.LabelRoleRelative, roleName)
	roles, err := k8sClient.RbacV1().ClusterRoles().List(metav1.ListOptions{
		LabelSelector: label,
	})

	if err != nil {
		apiHandler.WriteError(res, err)
		return
	}

	bigMap := make(map[string]map[string]api.RuleResource, 0)
	for _, role := range roles.Items {
		for _, rule := range role.Rules {
			for _, group := range rule.APIGroups {
				if group == "" {
					group = "core"
				}

				for _, resource := range rule.Resources {
					if _, ok := bigMap[group]; !ok {
						bigMap[group] = make(map[string]api.RuleResource, 0)
					}
					var resourceKey string
					if len(rule.ResourceNames) > 0 {
						resourceKey = resource + strings.Join(rule.ResourceNames, ",")
					} else {
						resourceKey = resource
					}

					if _, ok := bigMap[group][resourceKey]; !ok {
						bigMap[group][resourceKey] = api.RuleResource{
							Name:          resource,
							Verbs:         rule.Verbs,
							ResourceNames: rule.ResourceNames,
						}
					}
					verbs := bigMap[group][resourceKey].Verbs
					for _, verb := range rule.Verbs {
						if !util.StringInSlice(verb, verbs) {
							verbs = append(verbs, verb)
						}
					}

					bigMap[group][resourceKey] = api.RuleResource{
						Name:          resource,
						Verbs:         verbs,
						ResourceNames: rule.ResourceNames,
					}
				}

			}
		}
	}

	roleRuleList := api.RoleRuleList{
		TypeMeta: metav1.TypeMeta{
			Kind:       "RoleRulesList",
			APIVersion: "v1",
		},
		Items: make([]api.RoleRule, 0),
	}

	for group, resources := range bigMap {

		roleRule := api.RoleRule{
			Group:     group,
			Resources: make([]api.RuleResource, 0),
		}

		for _, resource := range resources {
			roleRule.Resources = append(roleRule.Resources, resource)
		}

		roleRuleList.Items = append(roleRuleList.Items, roleRule)
	}

	res.WriteHeaderAndEntity(http.StatusOK, roleRuleList)
}
