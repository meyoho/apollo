#!/bin/sh

argsargs="$@"

sed -i "s|{{ARGS}}|$argsargs|g" /etc/supervisord.conf

# Run supervisor foreground
/usr/bin/supervisord --nodaemon