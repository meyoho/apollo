// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"fmt"
	"k8s.io/client-go/dynamic"
	"log"
	"os/user"
	"path/filepath"
	"strings"

	restful "github.com/emicklei/go-restful"
	v1 "k8s.io/api/authorization/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/clientcmd/api"

	clientapi "bitbucket.org/mathildetech/apollo/client/api"
	"k8s.io/apimachinery/pkg/api/errors"

	authclientapi "bitbucket.org/mathildetech/apollo/pkg/auth/clientset/v1"
	clusterclientapi "bitbucket.org/mathildetech/apollo/pkg/auth/clientset/v1alpha1"
	authv1beta1client "bitbucket.org/mathildetech/apollo/pkg/auth/clientset/v1beta1"
)

// Dashboard UI default values for client configs.
const (
	// High enough QPS to fit all expected use cases. QPS=0 is not set here, because
	// client code is overriding it.
	DefaultQPS = 1e6
	// High enough Burst to fit all expected use cases. Burst=0 is not set here, because
	// client code is overriding it.
	DefaultBurst = 1e6
	// Use kubernetes protobuf as content type by default
	DefaultContentType = "application/vnd.kubernetes.protobuf"
	// Default cluster/context/auth name to be set in clientcmd config
	DefaultCmdConfigName = "kubernetes"
	// Default http header for user-agent
	DefaultUserAgent = "apollo"
)

// VERSION of this binary
var Version = "UNKNOWN"

// clientManager implements ClientManager interface
type clientManager struct {
	// Path to kubeconfig file. If both kubeConfigPath and apiserverHost are empty
	// inClusterConfig will be used
	kubeConfigPath string
	// Address of apiserver host in format 'protocol://address:port'
	apiserverHost string
	// Initialized on clientManager creation and used if kubeconfigPath and apiserverHost are
	// empty
	inClusterConfig *rest.Config
	// Responsible for decrypting tokens coming in request header. Used for authentication.
	//tokenManager authApi.TokenManager
	// Kubernetes client created without providing auth info. It uses permissions granted to
	// service account used by dashboard or kubeconfig file if it was passed during dashboard init.
	insecureClient kubernetes.Interface
	// Kubernetes client config created without providing auth info. It uses permissions granted
	// to service account used by dashboard or kubeconfig file if it was passed during dashboard
	// init.
	insecureConfig *rest.Config
}

func (self *clientManager) DynamicClient(req *restful.Request) (dynamic.Interface, error) {
	if req == nil {
		return nil, errors.NewBadRequest("request can not be nil")
	}

	cfg, err := self.Config(req)
	if err != nil {
		return nil, err
	}

	dynClient, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}

	return dynClient, nil
}

// Client returns a kubernetes client. In case dashboard login is enabled and option to skip
// login page is disabled only secure client will be returned, otherwise insecure client will be
// used.
func (self *clientManager) Client(req *restful.Request) (kubernetes.Interface, error) {
	if req == nil {
		return nil, errors.NewBadRequest("request can not be nil")
	}

	if self.IsSecureModeEnabled(req) {
		return self.secureClient(req)
	}

	return self.InsecureClient(), nil
}

func (self *clientManager) AuthV1Client(req *restful.Request) (authclientapi.AuthV1Interface, error) {
	cfg, err := self.Config(req)
	if err != nil {
		return nil, err
	}
	client, err := authclientapi.NewForConfig(cfg)
	if err != nil {
		return nil, fmt.Errorf("new auth client: %+v", err)
	}
	return client, nil
}

func (self *clientManager) AuthV1Beta1Client(req *restful.Request) (authv1beta1client.AuthV1Beta1Interface, error) {
	cfg, err := self.Config(req)
	if err != nil {
		return nil, err
	}
	client, err := authv1beta1client.NewForConfig(cfg)
	if err != nil {
		return nil, fmt.Errorf("new auth v1beta1 client: %+v", err)
	}
	return client, nil
}

func (self *clientManager) InsecureAuthV1Client() (authclientapi.AuthV1Interface, error) {
	client, err := authclientapi.NewForConfig(self.inClusterConfig)
	if err != nil {
		return nil, fmt.Errorf("new auth client: %+v", err)
	}
	return client, nil
}

func (self *clientManager) InsecureAuthV1Beta1Client() (authv1beta1client.AuthV1Beta1Interface, error) {
	client, err := authv1beta1client.NewForConfig(self.inClusterConfig)
	if err != nil {
		return nil, fmt.Errorf("new auth client: %+v", err)
	}
	return client, nil
}

func (self *clientManager) ClusterV1Alpha1Client(req *restful.Request) (clusterclientapi.ClusterV1Alpha1Interface, error) {
	cfg, err := self.Config(req)
	if err != nil {
		return nil, err
	}
	client, err := clusterclientapi.NewForConfig(cfg)
	if err != nil {
		return nil, fmt.Errorf("new clusters client: %+v", err)
	}
	return client, nil
}

func (self *clientManager) InsecureClusterV1Alpha1Client() (clusterclientapi.ClusterV1Alpha1Interface, error) {
	client, err := clusterclientapi.NewForConfig(self.inClusterConfig)
	if err != nil {
		return nil, fmt.Errorf("new clusters client: %+v", err)
	}
	return client, nil
}

// Config returns a rest config. In case dashboard login is enabled and option to skip
// login page is disabled only secure config will be returned, otherwise insecure config will be
// used.
func (self *clientManager) Config(req *restful.Request) (*rest.Config, error) {
	if req == nil {
		return nil, errors.NewBadRequest("request can not be nil")
	}
	//return self.secureConfig(req)

	// remove auto insecure cnf
	if self.IsSecureModeEnabled(req) {
		return self.secureConfig(req)
	}
	return self.InsecureConfig(), nil
}

// InsecureClient returns kubernetes client that was created without providing auth info. It uses
// permissions granted to service account used by dashboard or kubeconfig file if it was passed
// during dashboard init.
func (self *clientManager) InsecureClient() kubernetes.Interface {
	return self.insecureClient
}

// InsecureConfig returns kubernetes client config that used privileges of dashboard service account
// or kubeconfig file if it was passed during dashboard init.
func (self *clientManager) InsecureConfig() *rest.Config {
	return self.insecureConfig
}

// CanI returns true when user is allowed to access data provided within SelfSubjectAccessReview, false otherwise.
func (self *clientManager) CanI(req *restful.Request, ssar *v1.SelfSubjectAccessReview) bool {
	// In case user is not authenticated (uses skip option) do not allow access.
	info, _ := self.extractAuthInfo(req)
	if info == nil {
		return false
	}

	client, err := self.Client(req)
	if err != nil {
		log.Println(err)
		return false
	}

	response, err := client.AuthorizationV1().SelfSubjectAccessReviews().Create(ssar)
	if err != nil {
		log.Println(err)
		return false
	}

	return response.Status.Allowed
}

// ClientCmdConfig creates ClientCmd Config based on authentication information extracted from request.
// Currently request header is only checked for existence of 'Authentication: BearerToken'
func (self *clientManager) ClientCmdConfig(req *restful.Request) (clientcmd.ClientConfig, error) {
	authInfo, err := self.extractAuthInfo(req)
	if err != nil {
		return nil, err
	}

	cfg, err := self.buildConfigFromFlags(self.apiserverHost, self.kubeConfigPath)
	if err != nil {
		return nil, err
	}

	return self.buildCmdConfig(authInfo, cfg), nil
}

// HasAccess configures K8S api client with provided auth info and executes a basic check against apiserver to see
// if it is valid.
func (self *clientManager) HasAccess(authInfo api.AuthInfo) error {
	cfg, err := self.buildConfigFromFlags(self.apiserverHost, self.kubeConfigPath)
	if err != nil {
		return err
	}

	clientConfig := self.buildCmdConfig(&authInfo, cfg)
	cfg, err = clientConfig.ClientConfig()
	if err != nil {
		return err
	}

	client, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return err
	}

	_, err = client.ServerVersion()
	return err
}

// VerberClient returns new verber client based on authentication information extracted from request
func (self *clientManager) VerberClient(req *restful.Request) (clientapi.ResourceVerber, error) {
	client, err := self.Client(req)
	if err != nil {
		return nil, err
	}

	return NewResourceVerber(client.CoreV1().RESTClient(),
		client.ExtensionsV1beta1().RESTClient(), client.AppsV1().RESTClient(),
		client.BatchV1().RESTClient(), client.BatchV1beta1().RESTClient(), client.AutoscalingV1().RESTClient(),
		client.StorageV1().RESTClient(), client.RbacV1().RESTClient()), nil
}

// SetTokenManager sets the token manager that will be used for token decryption.
//func (self *clientManager) SetTokenManager(manager authApi.TokenManager) {
//	self.tokenManager = manager
//}

// Initializes config with default values
func (self *clientManager) initConfig(cfg *rest.Config) {
	cfg.QPS = DefaultQPS
	cfg.Burst = DefaultBurst
	cfg.ContentType = DefaultContentType
	cfg.UserAgent = DefaultUserAgent + "/" + Version
}

// Returns rest Config based on provided apiserverHost and kubeConfigPath flags. If both are
// empty then in-cluster config will be used and if it is nil the error is returned.
func (self *clientManager) buildConfigFromFlags(apiserverHost, kubeConfigPath string) (
	*rest.Config, error) {
	if len(kubeConfigPath) > 0 || len(apiserverHost) > 0 {
		return clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
			&clientcmd.ClientConfigLoadingRules{ExplicitPath: kubeConfigPath},
			&clientcmd.ConfigOverrides{ClusterInfo: api.Cluster{Server: apiserverHost}}).ClientConfig()
	}

	if self.isRunningInCluster() {
		return self.inClusterConfig, nil
	}
	return nil, errors.NewUnauthorized("could not create client config")
}

// Based on auth info and rest config creates client cmd config.
func (self *clientManager) buildCmdConfig(authInfo *api.AuthInfo, cfg *rest.Config) clientcmd.ClientConfig {
	cmdCfg := api.NewConfig()
	cmdCfg.Clusters[DefaultCmdConfigName] = &api.Cluster{
		Server:                   cfg.Host,
		CertificateAuthority:     cfg.TLSClientConfig.CAFile,
		CertificateAuthorityData: cfg.TLSClientConfig.CAData,
		InsecureSkipTLSVerify:    cfg.TLSClientConfig.Insecure,
	}
	cmdCfg.AuthInfos[DefaultCmdConfigName] = authInfo
	cmdCfg.Contexts[DefaultCmdConfigName] = &api.Context{
		Cluster:  DefaultCmdConfigName,
		AuthInfo: DefaultCmdConfigName,
	}
	cmdCfg.CurrentContext = DefaultCmdConfigName

	return clientcmd.NewDefaultClientConfig(
		*cmdCfg,
		&clientcmd.ConfigOverrides{},
	)
}

// Extracts authorization information from the request header
func (self *clientManager) extractAuthInfo(req *restful.Request) (*api.AuthInfo, error) {
	authHeader := req.HeaderParameter("Authorization")

	// Authorization header will be more important than our token
	token := self.extractTokenFromHeader(authHeader)
	if len(token) > 0 {
		return &api.AuthInfo{Token: token}, nil
	}

	return nil, errors.NewUnauthorized("")
}

// Checks if request headers contain any auth information without parsing.
func (self *clientManager) containsAuthInfo(req *restful.Request) bool {
	authHeader := req.HeaderParameter("Authorization")

	return len(authHeader) > 0
}

func (self *clientManager) extractTokenFromHeader(authHeader string) string {
	if strings.HasPrefix(authHeader, "Bearer ") {
		return strings.TrimPrefix(authHeader, "Bearer ")
	}

	return ""
}

func (self *clientManager) isLoginEnabled(req *restful.Request) bool {
	return req.Request.TLS != nil
}

// Secure mode means that every request to Dashboard has to be authenticated and privileges
// of Dashboard SA can not be used.
func (self *clientManager) IsSecureModeEnabled(req *restful.Request) bool {
	if self.containsAuthInfo(req) {
		return true
	}

	if self.isLoginEnabled(req) {
		return true
	}

	return self.isLoginEnabled(req) && self.containsAuthInfo(req)
}

func (self *clientManager) secureClient(req *restful.Request) (kubernetes.Interface, error) {
	cfg, err := self.secureConfig(req)
	if err != nil {
		return nil, err
	}

	client, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (self *clientManager) secureConfig(req *restful.Request) (*rest.Config, error) {
	cmdConfig, err := self.ClientCmdConfig(req)
	if err != nil {
		return nil, err
	}

	cfg, err := cmdConfig.ClientConfig()
	if err != nil {
		return nil, err
	}

	self.initConfig(cfg)
	return cfg, nil
}

// Initializes client manager
func (self *clientManager) init() {
	self.initInClusterConfig()
	self.initInsecureClient()
}

// Initializes in-cluster config if apiserverHost and kubeConfigPath were not provided.
func (self *clientManager) initInClusterConfig() {
	if len(self.apiserverHost) > 0 || len(self.kubeConfigPath) > 0 {
		log.Print("Skipping in-cluster config")
		return
	}

	log.Print("Using in-cluster config to connect to apiserver")

	// Insecure
	cfg, err := self.GetConfig()
	if err != nil {
		log.Printf("Could not init in cluster config: %s", err.Error())
		return
	}

	self.inClusterConfig = cfg
}

func (self *clientManager) GetConfig() (*rest.Config, error) {

	// If no explicit location, try the in-cluster config
	if c, err := rest.InClusterConfig(); err == nil {
		return c, nil
	}
	// If no in-cluster config, try the default location in the user's home directory
	if usr, err := user.Current(); err == nil {
		if c, err := clientcmd.BuildConfigFromFlags(
			"", filepath.Join(usr.HomeDir, ".kube", "config")); err == nil {
			return c, nil
		}
	}

	return nil, fmt.Errorf("could not locate a kubeconfig")
}

func (self *clientManager) initInsecureClient() {
	self.initInsecureConfig()
	client, err := kubernetes.NewForConfig(self.insecureConfig)
	if err != nil {
		panic(err)
	}

	self.insecureClient = client
}

func (self *clientManager) initInsecureConfig() {
	cfg, err := self.buildConfigFromFlags(self.apiserverHost, self.kubeConfigPath)
	if err != nil {
		panic(err)
	}

	self.initConfig(cfg)
	self.insecureConfig = cfg
}

// Returns true if in-cluster config is used
func (self *clientManager) isRunningInCluster() bool {
	return self.inClusterConfig != nil
}

// NewClientManager creates client manager based on kubeConfigPath and apiserverHost parameters.
// If both are empty then in-cluster config is used.
func NewClientManager(kubeConfigPath, apiserverHost string) clientapi.ClientManager {
	result := &clientManager{
		kubeConfigPath: kubeConfigPath,
		apiserverHost:  apiserverHost,
	}

	result.init()
	return result
}
