/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"sigs.k8s.io/controller-runtime/pkg/client"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var logger = logf.Log.WithName("clusterregistry")

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ClusterSpec defines the desired state of Cluster
type ClusterSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// ClusterStatus defines the observed state of Cluster
type ClusterStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Cluster is the Schema for the clusters API
// +k8s:openapi-gen=true
type Cluster struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ClusterSpec   `json:"spec,omitempty"`
	Status ClusterStatus `json:"status,omitempty"`
}

type ClusterAttr struct {
	Kubernetes KubernetesInfo `json:"kubernetes,omitempty"`
}

type KubernetesInfo struct {
	Type     string            `json:"type,omitempty"`
	Cni      map[string]string `json:"cni,omitempty"`
	Version  string            `json:"version,omitempty"`
	Endpoint string            `json:"endpoint,omitempty"`
	Token    string            `json:"token,omitempty"`
}

func (c *Cluster) GetClusterNameAndToken() (clusterName string, token string, err error) {
	var (
		attrStr     string
		clusterAttr ClusterAttr
	)
	attrStr, ok := c.Annotations[constant.AnnotationClusterAttr]
	if !ok {
		return "", "", fmt.Errorf("Cluster attr does not exist")
	}
	if err := json.Unmarshal([]byte(attrStr), &clusterAttr); err != nil {
		return "", "", err
	}
	return c.Name, clusterAttr.Kubernetes.Token, nil
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterList contains a list of Cluster
type ClusterList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Cluster `json:"items"`
}

//func init() {
//	SchemeBuilder.Register(&Cluster{}, &ClusterList{})
//}

func ListClusters(cli client.Client) []Cluster {
	list := &ClusterList{}
	leaderElectionNamespace := GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	if err := cli.List(context.TODO(), &client.ListOptions{Namespace: leaderElectionNamespace}, list); err != nil {
		logger.Error(err, "List clusters")
	}
	return list.Items
}

// GetEnv get value from os environment, if not available, then return fallback.
func GetEnv(key, fallback string) string {
	value, exists := os.LookupEnv(key)
	if exists && len(value) != 0 {
		return value
	}
	return fallback
}
