package api

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/emicklei/go-restful"
)

type JWEToken struct {
	Issuer        string      `json:"iss"`
	Subject       string      `json:"sub"`
	Audience      string      `json:"aud"`
	Expiry        int         `json:"exp"`
	IssuedAt      int         `json:"iat"`
	Nonce         string      `json:"nonce"`
	Email         string      `json:"email"`
	EmailVerified bool        `json:"email_verified"`
	Name          string      `json:"name"`
	Groups        []string    `json:"groups"`
	Ext           jwtTokenExt `json:"ext"`
	MetadataName  string
}

type jwtTokenExt struct {
	IsAdmin bool   `json:"is_admin"`
	ConnID  string `json:"conn_id"`
}

func ParseJWTFromHeader(request *restful.Request) (*JWEToken, error) {
	var rawToken string
	authorization := request.HeaderParameter("Authorization")
	if strings.TrimSpace(authorization) == "" {
		return nil, errors.New("Authentication head does not exist")
	}
	fmt.Printf("authorization header: %s", authorization)
	switch {
	case strings.HasPrefix(strings.TrimSpace(authorization), "Bearer"):
		rawToken = strings.TrimPrefix(strings.TrimSpace(authorization), "Bearer")
	case strings.HasPrefix(strings.TrimSpace(authorization), "bearer"):
		rawToken = strings.TrimPrefix(strings.TrimSpace(authorization), "bearer")
	}
	return ParseJWT(rawToken)
}

func ParseJWT(rawToken string) (*JWEToken, error) {
	var (
		token JWEToken
	)

	if rawToken == "" {
		return nil, errors.New("Authentication head is invalid")
	}
	parts := strings.Split(rawToken, ".")
	if len(parts) < 2 {
		return nil, fmt.Errorf("oidc: malformed jwt, expected 3 parts got %d", len(parts))
	}
	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, fmt.Errorf("oidc: malformed jwt payload: %v", err)
	}
	if err := json.Unmarshal(payload, &token); err != nil {
		fmt.Println(err)
		return nil, err
	}

	return &token, nil
}
