module bitbucket.org/mathildetech/apollo

go 1.12

require (
	alauda.io/warpgate v0.0.1
	bitbucket.org/mathildetech/alauda-backend v0.1.8
	bitbucket.org/mathildetech/app v1.0.1
	bitbucket.org/mathildetech/auth-controller2 v1.8.1-0.20200305032212-81937eea516b
	bitbucket.org/mathildetech/dex v2.6.1-0.20200306090351-89e53999db18+incompatible
	github.com/beevik/etree v1.1.0
	github.com/btcsuite/btcutil v0.0.0-20190425235716-9e5f4b9a998d
	github.com/coreos/go-oidc v2.0.0+incompatible
	github.com/emicklei/go-restful v2.9.6+incompatible
	github.com/ghodss/yaml v1.0.0
	github.com/go-logr/zapr v0.1.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/gtank/cryptopasta v0.0.0-20170601214702-1f550f6f2f69
	github.com/juju/errors v0.0.0-20181118221551-089d3ea4e4d5
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8 // indirect
	github.com/juju/testing v0.0.0-20191001232224-ce9dec17d28b // indirect
	github.com/kylelemons/godebug v1.1.0
	github.com/onsi/gomega v1.7.0
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	github.com/russellhaering/goxmldsig v0.0.0-20180430223755-7acd5e4a6ef7
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.4.0
	go.uber.org/zap v1.10.0
	golang.org/x/crypto v0.0.0-20191105034135-c7e5f84aec59
	golang.org/x/net v0.0.0-20191105084925-a882066a44e0
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	google.golang.org/grpc v1.21.0
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/ldap.v3 v3.0.3
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	gopkg.in/square/go-jose.v2 v2.3.1
	k8s.io/api v0.0.0-20190313235455-40a48860b5ab
	k8s.io/apimachinery v0.0.1
	k8s.io/client-go v11.0.0+incompatible
	sigs.k8s.io/controller-runtime v0.1.11
)

replace github.com/prometheus/client_golang => github.com/prometheus/client_golang v0.9.3

replace k8s.io/apimachinery => k8s.io/apimachinery v0.0.0-20190313205120-d7deff9243b1

replace alauda.io/warpgate v0.0.1 => bitbucket.org/mathildetech/warpgate v0.0.0-20191106063512-027fc7866460
